﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)T!!!*Q(C=\&gt;5\4M.1%)8B%U2"E=9A?J1N4%G&lt;*:!NT"&lt;3YCYNZ7QB.6WW-&amp;P)&amp;L)&amp;]^_&lt;)5*#E!911PB[]DC_DQ`&lt;-6,@LK2,4??W_;@&lt;[`(J7&amp;/P;8Y;^_;^\@0;Z^/\]=@W]@(80J]&gt;0UXR\PD(`D/&lt;`L@`\7^PDW&gt;`".`_/ZCJ09CIJ)*SSN37H:)]S:-]S:-]S9-]S)-]S)-]S*X=S:X=S:X=S9X=S)X=S)X=S(MD&amp;\H)21YJ74R:+*EUG3$J$%8*3_**0)EH]@"6C3@R**\%EXDIIM34?"*0YEE]$&amp;0C34S**`%E(K&lt;KEOS.(%`C98I&amp;HM!4?!*0Y'&amp;*":Y!%#Q74"R-!E0"Q?"$Y!E]A9?0#DS"*`!%HM$$917?Q".Y!E`A95A`+^%VL:(D92IZ(M@D?"S0YW&amp;K/2\(YXA=D_.B/4E?R_-AH!7&gt;S3()'?2U=,YY(M@$GRS0YX%]DM@R=+B@)?^HJGF;)]&gt;D?!S0Y4%]BI=J:(A-D_%R0);(;76Y$)`B-4S'B[6E?!S0Y4%ARK)M,W-S9[$2S1A-$XO`7[R@J?A3[UWKGV&gt;V5[JO.N6.J,IZ6"&gt;&gt;&gt;4&amp;6&amp;UFV]F5H687S6#&gt;"^=?JU#K-;B(6Y.:2"V\XV)\;5BNK4;WI*&lt;7ABD&lt;UCTM?$A@N^XPN&gt;DNNNVNN.BONVWON6CMNFUMN&amp;AM.QX"[$.T24A_%YX0JE=`DT@UY0D`.RA@K&gt;JC.VU_TFP`!`_&gt;@]'T5B&gt;[OQ4F[!92=.X!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 56 48 49 56 48 49 49 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 17 2 1 100 0 0 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 153 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 3 188 0 0 0 0 0 0 0 0 0 0 3 162 0 40 0 0 3 156 0 0 3 96 0 0 0 0 0 9 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 6 66 97 110 110 101 114 100 1 0 0 0 0 0 4 77 81 84 84 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arguments" Type="Folder">
			<Item Name="Request" Type="Folder">
				<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../Stop Argument--cluster.ctl"/>
				<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../Show Panel Argument--cluster.ctl"/>
				<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../Hide Panel Argument--cluster.ctl"/>
				<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../Show Diagram Argument--cluster.ctl"/>
				<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../Get Module Execution Status Argument--cluster.ctl"/>
				<Item Name="Configure Argument--cluster.ctl" Type="VI" URL="../Configure Argument--cluster.ctl"/>
				<Item Name="Prepare Argument--cluster.ctl" Type="VI" URL="../Prepare Argument--cluster.ctl"/>
				<Item Name="Set Client Parameters Argument--cluster.ctl" Type="VI" URL="../Set Client Parameters Argument--cluster.ctl"/>
				<Item Name="Set Connection Parameters Argument--cluster.ctl" Type="VI" URL="../Set Connection Parameters Argument--cluster.ctl"/>
				<Item Name="Connect Argument--cluster.ctl" Type="VI" URL="../Connect Argument--cluster.ctl"/>
				<Item Name="Connect (Reply Payload)--cluster.ctl" Type="VI" URL="../Connect (Reply Payload)--cluster.ctl"/>
				<Item Name="Disconnect Argument--cluster.ctl" Type="VI" URL="../Disconnect Argument--cluster.ctl"/>
				<Item Name="Disconnect (Reply Payload)--cluster.ctl" Type="VI" URL="../Disconnect (Reply Payload)--cluster.ctl"/>
				<Item Name="Publish Argument--cluster.ctl" Type="VI" URL="../Publish Argument--cluster.ctl"/>
				<Item Name="Subscribe Argument--cluster.ctl" Type="VI" URL="../Subscribe Argument--cluster.ctl"/>
				<Item Name="Unsubscribe Argument--cluster.ctl" Type="VI" URL="../Unsubscribe Argument--cluster.ctl"/>
				<Item Name="Start Client Argument--cluster.ctl" Type="VI" URL="../Start Client Argument--cluster.ctl"/>
				<Item Name="Start Client (Reply Payload)--cluster.ctl" Type="VI" URL="../Start Client (Reply Payload)--cluster.ctl"/>
				<Item Name="Stop Client Argument--cluster.ctl" Type="VI" URL="../Stop Client Argument--cluster.ctl"/>
				<Item Name="Stop Client (Reply Payload)--cluster.ctl" Type="VI" URL="../Stop Client (Reply Payload)--cluster.ctl"/>
			</Item>
			<Item Name="Broadcast" Type="Folder">
				<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../Did Init Argument--cluster.ctl"/>
				<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../Status Updated Argument--cluster.ctl"/>
				<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../Error Reported Argument--cluster.ctl"/>
				<Item Name="Application Message Argument--cluster.ctl" Type="VI" URL="../Application Message Argument--cluster.ctl"/>
			</Item>
		</Item>
		<Item Name="Requests" Type="Folder">
			<Item Name="Show Panel.vi" Type="VI" URL="../Show Panel.vi"/>
			<Item Name="Hide Panel.vi" Type="VI" URL="../Hide Panel.vi"/>
			<Item Name="Stop Module.vi" Type="VI" URL="../Stop Module.vi"/>
			<Item Name="Show Diagram.vi" Type="VI" URL="../Show Diagram.vi"/>
			<Item Name="Configure.vi" Type="VI" URL="../Configure.vi"/>
			<Item Name="Prepare.vi" Type="VI" URL="../Prepare.vi"/>
			<Item Name="Set Client Parameters.vi" Type="VI" URL="../Set Client Parameters.vi"/>
			<Item Name="Start Client.vi" Type="VI" URL="../Start Client.vi"/>
			<Item Name="Set Connection Parameters.vi" Type="VI" URL="../Set Connection Parameters.vi"/>
			<Item Name="Connect.vi" Type="VI" URL="../Connect.vi"/>
			<Item Name="Disconnect.vi" Type="VI" URL="../Disconnect.vi"/>
			<Item Name="Publish.vi" Type="VI" URL="../Publish.vi"/>
			<Item Name="Subscribe.vi" Type="VI" URL="../Subscribe.vi"/>
			<Item Name="Unsubscribe.vi" Type="VI" URL="../Unsubscribe.vi"/>
			<Item Name="Stop Client.vi" Type="VI" URL="../Stop Client.vi"/>
		</Item>
		<Item Name="Start Module.vi" Type="VI" URL="../Start Module.vi"/>
		<Item Name="Synchronize Module Events.vi" Type="VI" URL="../Synchronize Module Events.vi"/>
		<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../Obtain Broadcast Events for Registration.vi"/>
	</Item>
	<Item Name="Broadcasts" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../Broadcast Events--cluster.ctl"/>
		<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../Obtain Broadcast Events.vi"/>
		<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../Destroy Broadcast Events.vi"/>
		<Item Name="Module Did Init.vi" Type="VI" URL="../Module Did Init.vi"/>
		<Item Name="Status Updated.vi" Type="VI" URL="../Status Updated.vi"/>
		<Item Name="Error Reported.vi" Type="VI" URL="../Error Reported.vi"/>
		<Item Name="Module Did Stop.vi" Type="VI" URL="../Module Did Stop.vi"/>
		<Item Name="Update Module Execution Status.vi" Type="VI" URL="../Update Module Execution Status.vi"/>
		<Item Name="Application Message.vi" Type="VI" URL="../Application Message.vi"/>
	</Item>
	<Item Name="Requests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Request Events--cluster.ctl" Type="VI" URL="../Request Events--cluster.ctl"/>
		<Item Name="Obtain Request Events.vi" Type="VI" URL="../Obtain Request Events.vi"/>
		<Item Name="Destroy Request Events.vi" Type="VI" URL="../Destroy Request Events.vi"/>
		<Item Name="Get Module Execution Status.vi" Type="VI" URL="../Get Module Execution Status.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Close Module.vi" Type="VI" URL="../Close Module.vi"/>
		<Item Name="Handle Exit.vi" Type="VI" URL="../Handle Exit.vi"/>
		<Item Name="Hide VI Panel.vi" Type="VI" URL="../Hide VI Panel.vi"/>
		<Item Name="Init Module.vi" Type="VI" URL="../Init Module.vi"/>
		<Item Name="Module Data--cluster.ctl" Type="VI" URL="../Module Data--cluster.ctl"/>
		<Item Name="Module Name--constant.vi" Type="VI" URL="../Module Name--constant.vi"/>
		<Item Name="Module Not Running--error.vi" Type="VI" URL="../Module Not Running--error.vi"/>
		<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../Module Not Stopped--error.vi"/>
		<Item Name="Module Not Synced--error.vi" Type="VI" URL="../Module Not Synced--error.vi"/>
		<Item Name="Module Running as Cloneable--error.vi" Type="VI" URL="../Module Running as Cloneable--error.vi"/>
		<Item Name="Module Running as Singleton--error.vi" Type="VI" URL="../Module Running as Singleton--error.vi"/>
		<Item Name="Request and Wait for Reply Timeout--error.vi" Type="VI" URL="../Request and Wait for Reply Timeout--error.vi"/>
		<Item Name="Master Reference Not Closed--error.vi" Type="VI" URL="../Master Reference Not Closed--error.vi"/>
		<Item Name="Module Timeout--constant.vi" Type="VI" URL="../Module Timeout--constant.vi"/>
		<Item Name="Open VI Panel.vi" Type="VI" URL="../Open VI Panel.vi"/>
	</Item>
	<Item Name="Module Sync" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Semaphore" Type="Folder">
			<Item Name="Obtain Module Semaphore.vi" Type="VI" URL="../Obtain Module Semaphore.vi"/>
			<Item Name="Acquire Module Semaphore.vi" Type="VI" URL="../Acquire Module Semaphore.vi"/>
			<Item Name="Release Module Semaphore.vi" Type="VI" URL="../Release Module Semaphore.vi"/>
			<Item Name="Destroy Module Semaphore Reference.vi" Type="VI" URL="../Destroy Module Semaphore Reference.vi"/>
		</Item>
		<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../Destroy Sync Refnums.vi"/>
		<Item Name="Get Sync Refnums.vi" Type="VI" URL="../Get Sync Refnums.vi"/>
		<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../Synchronize Caller Events.vi"/>
		<Item Name="Wait on Event Sync.vi" Type="VI" URL="../Wait on Event Sync.vi"/>
		<Item Name="Wait on Module Sync.vi" Type="VI" URL="../Wait on Module Sync.vi"/>
		<Item Name="Wait on Stop Sync.vi" Type="VI" URL="../Wait on Stop Sync.vi"/>
	</Item>
	<Item Name="Multiple Instances" Type="Folder">
		<Item Name="Module Ring" Type="Folder">
			<Item Name="Init Select Module Ring.vi" Type="VI" URL="../Init Select Module Ring.vi"/>
			<Item Name="Update Select Module Ring.vi" Type="VI" URL="../Update Select Module Ring.vi"/>
			<Item Name="Addressed to This Module.vi" Type="VI" URL="../Addressed to This Module.vi"/>
		</Item>
		<Item Name="VI Reference Management.lvlib" Type="Library" URL="../VI Reference Management/VI Reference Management.lvlib"/>
		<Item Name="Clone Registration.lvlib" Type="Library" URL="../Clone Registration/Clone Registration.lvlib"/>
		<Item Name="Test Clone Registration API.vi" Type="VI" URL="../Clone Registration/Test Clone Registration API.vi"/>
		<Item Name="Get Module Running State.vi" Type="VI" URL="../Get Module Running State.vi"/>
		<Item Name="Is Safe to Destroy Refnums.vi" Type="VI" URL="../Is Safe to Destroy Refnums.vi"/>
		<Item Name="Module Running State--enum.ctl" Type="VI" URL="../Module Running State--enum.ctl"/>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Item Name="Helper Loop Data--cluster.ctl" Type="VI" URL="../SubVIs/Helper Loop Data--cluster.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Helper Loop - Create Client Object.vi" Type="VI" URL="../SubVIs/Helper Loop - Create Client Object.vi"/>
		<Item Name="Helper Loop - Evaluate Connect Results.vi" Type="VI" URL="../SubVIs/Helper Loop - Evaluate Connect Results.vi"/>
		<Item Name="Helper Loop - Evaluate Disconnect Results.vi" Type="VI" URL="../SubVIs/Helper Loop - Evaluate Disconnect Results.vi"/>
		<Item Name="Helper Loop - Update Connection Parameters.vi" Type="VI" URL="../SubVIs/Helper Loop - Update Connection Parameters.vi"/>
	</Item>
	<Item Name="Main.vi" Type="VI" URL="../Main.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
