// Wenn der Arduino im Reset-Loop hängt, weil der Programmierer zu doof war...
// 
// Did you try the "double tap" on the reset button ?
//
// That places it in bootloader mode and often allows you to upload a sketch again.
// It may show a different port when you do that so use the new one for an upload and you have to double tap quite quickly too.



#include <ArduinoJson.h>

struct t_Payload_Analysis {
  bool is_broadcast;
  bool is_scene_change;
  bool is_scene_update;
  bool is_not_valid;
};

enum t_colortype{
  defaultcolor_1,
  defaultcolor_2,
  defaultcolor_3,
  custom_color
};

enum t_SceneParams_ProgressDirection {
  linear,
  bidirectional
};

struct t_SceneParams_Progress {
  t_SceneParams_ProgressDirection prog_dir;
  t_colortype base_color;
  byte custombase_rgb[3];
  t_colortype fill_color;
  byte customfill_rgb[3];
  int prog_percent;
};






t_Payload_Analysis      Payload_Analysis;
t_SceneParams_Progress  ProgressParams;

char NewCommand[32] = "";                                  // Achtung: Limitiert die möglichen Längen für Broadcast- und Szenen-Namen!!!
char NewParams[1024] = "";                                 // Und auch das New-Params-Objekt ist limitiert. Der Einfachheit halber wie das ursprüngliche JSON-Dokument
                                                           // ToDo: Diese Max-Längen als Defines ganz oben festlegen!


// resets the payload analysis type
void ResetPayloadStruct(t_Payload_Analysis *_result) {
  _result->is_broadcast = false;
  _result->is_scene_change = false;
  _result->is_scene_update = false;
  _result->is_not_valid = false;
}



bool UpdateProgressJSON(char *sub_obj, t_SceneParams_Progress *_params) {

  DynamicJsonDocument doc(1024);
  DeserializationError error = deserializeJson(doc, sub_obj);

  if (error) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());

    // Abbruch 
    return false;
  }

  _params->prog_percent = doc["progress"] | _params->prog_percent;    // Ursprünglichen Wert hinter | heißt: verwende den als default, wenn ["progress"] nicht gefunden


  const char* _dir = doc["direction_progress"];                       // der Char-Array wird nach Funktionsende verworfen

  if (strcmp(_dir, "linear") == 0) _params->prog_dir = linear;        // nur überschreiben, wenn geändert -- wurde ["direction_progress"] nicht übertragen, ist _dir==""
  if (strcmp(_dir, "bi-dir") == 0) _params->prog_dir = bidirectional; // ---------"------------------

  const char* _baseColType = doc["base_color"]["color_type"];
  if (strcmp(_baseColType, "defaultcolor_1") == 0) _params->base_color = defaultcolor_1;
  if (strcmp(_baseColType, "defaultcolor_2") == 0) _params->base_color = defaultcolor_2;
  if (strcmp(_baseColType, "defaultcolor_3") == 0) _params->base_color = defaultcolor_3;
  if (strcmp(_baseColType, "custom") == 0)         _params->base_color = custom_color;

  
  for (int i = 0; i <= 2; i++) {
    _params->custombase_rgb[i] = doc["base_color"]["colorcode_RGB"][i] | _params->custombase_rgb[i];
  };
  
  const char* _fillColType = doc["fill_color"]["color_type"];                                 // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
  if (strcmp(_fillColType, "defaultcolor_1") == 0) _params->fill_color = defaultcolor_1;
  if (strcmp(_fillColType, "defaultcolor_2") == 0) _params->fill_color = defaultcolor_2;
  if (strcmp(_fillColType, "defaultcolor_3") == 0) _params->fill_color = defaultcolor_3;
  if (strcmp(_fillColType, "custom") == 0)         _params->fill_color = custom_color;

  
  for (int i = 0; i <= 2; i++) {
    _params->customfill_rgb[i] = doc["fill_color"]["colorcode_RGB"][i] | _params->custombase_rgb[i];
  };
  
  return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
}




// determines the payload type and slices the JSON object
void CheckJSONPayload(char *payload, t_Payload_Analysis *_result, char *_newcmd, char *_newparms) {

  ResetPayloadStruct(_result);
  
  StaticJsonDocument<1024> doc;
  DeserializationError error = deserializeJson(doc, payload);

  if (error) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());

    // Abbruch 
    _result->is_not_valid = true;
    return;
  }

  JsonObject obj = doc.as<JsonObject>();

  for (JsonPair p : obj) {
    if (p.key() == "broadcast") {
      Serial.println("Broadcast detected!");
      const char* _bc_msg = doc["broadcast"]["bc_message"];
      if (_bc_msg != nullptr) {
        _result->is_broadcast = true;
        strlcpy(_newcmd, _bc_msg, 32);
        serializeJson(obj["broadcast"]["bc_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
        return;  
      }
      else {
        Serial.println("no 'bc_message' key found!");
        _result->is_not_valid = true;
        return;
      }
    }
    if (p.key() == "change_scene") {
      Serial.println("Scene Change Request detected!");
      const char* _sc_msg = doc["change_scene"]["new_scene"];
      if (_sc_msg != nullptr) {
        _result->is_scene_change = true;
        strlcpy(_newcmd, _sc_msg, 32);
        serializeJson(obj["change_scene"]["new_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
        return;  
      }
      else {
        Serial.println("no 'new_scene' key found!");
        _result->is_not_valid = true;
        return;
      }
    }
    if (p.key() == "update_scene") {
      Serial.println("Scene Update detected!");
      _result->is_scene_update = true;
      serializeJson(obj["update_scene"]["new_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
      return;  
     }
   }
}



void setup() {
  // Initialize serial port
  Serial.begin(9600);
  while (!Serial) continue;
  Serial.println("");
  Serial.println("=========================================================================================");



  ProgressParams.prog_dir = bidirectional;           // nur um zu sehen, dass newScene das überschreibt
  ProgressParams.base_color = defaultcolor_1;
  ProgressParams.custombase_rgb[0] = 0;
  ProgressParams.custombase_rgb[1] = 0;
  ProgressParams.custombase_rgb[2] = 0;
  ProgressParams.fill_color = defaultcolor_2;
  ProgressParams.customfill_rgb[0] = 0;
  ProgressParams.customfill_rgb[1] = 0;
  ProgressParams.customfill_rgb[2] = 0;
  ProgressParams.prog_percent = 0;


  // char Payload[] = "{\"broadcast\": {\"bc_message\": \"all_off\", \"bc_params\": {}}}";
  // char Payload[] = "{\"broadcast\": {\"bc_message\": \"reset\", \"bc_params\": {}}}";
  // char Payload[] = "{\"change_scene\":{\"new_scene\":\"progress\",\"new_params\":{\"direction_progress\":\"linear\",\"base_color\":{\"color_type\":\"custom\",\"colorcode_RGB\":[0,0,32]},\"fill_color\":{\"color_type\":\"defaultcolor_3\"},\"progress\":0}}}";
 char Payload[] = "\"{\"update_scene\": {\"new_params\": {\"progress\": 5 }}}\"";



  CheckJSONPayload(Payload, &Payload_Analysis, NewCommand, NewParams);

  if (Payload_Analysis.is_broadcast) {
    Serial.print("Broadcast received in caller. ");

    if (strcmp(NewCommand, "reset") == 0) {                      
      Serial.println("It is a reset broadcast!");
    }
    if (strcmp(NewCommand, "all_off") == 0) {
      Serial.println("It is an all-off broadcast!");
    }
  }

  if (Payload_Analysis.is_scene_change) {
    Serial.print("Scene change request received in caller.\nNew scene: ");
    Serial.println(NewCommand);
    Serial.print("New parameters: ");
    Serial.println(NewParams);
  }

  if (strcmp(NewCommand, "progress") == 0) {   // ganz doof mal dahintergehängt, käme in die Szene

    Serial.print("Old base color type: ");
    Serial.println(ProgressParams.base_color);

    // scene-specific parsing of JSON sub-object
    UpdateProgressJSON(NewParams, &ProgressParams);

    Serial.print("New base color type: ");
    Serial.println(ProgressParams.base_color);
      
  }

  
  if (Payload_Analysis.is_scene_update) {     // auch doofe Demo

    Serial.print("Old progress: ");
    Serial.println(ProgressParams.prog_percent);

    // scene-specific parsing of JSON sub-object
    UpdateProgressJSON(NewParams, &ProgressParams);

    Serial.print("New progress: ");
    Serial.println(ProgressParams.prog_percent); 

  }
  
  
}

void loop() {
  // not used in this example
}
