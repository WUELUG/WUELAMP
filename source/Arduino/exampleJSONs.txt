{
  "change_scene":{
    "new_scene":"twinkle" ,
    "new_params":{
      "period":200,
      "brightness1":100,
      "brightness2":10,
      "color1":{"color_type":"defaultcolor_2","colorcode_RGB":[]},
      "color2":{"color_type":"custom","colorcode_RGB":[255,255,255]}
    }
  }
}

{
  "change_scene":{
    "new_scene":"loading" ,
    "new_params":{
      "color1":{"color_type":"defaultcolor_3","colorcode_RGB":[]},
      "period":2000,
      "brightness1":100,
      "length":33,
      "direction":false
    }
  }
}

{
  "change_scene":{
    "new_scene":"static" ,
    "new_params":{
      "color1":{"color_type":"custom","colorcode_RGB":[8,255,0]},
      "brightness1":100
    }
  }
}

{
  "change_scene":{
    "new_scene":"progress" ,
    "new_params":{
      "color1":{"color_type":"defaultcolor_1","colorcode_RGB":[8,255,0]},
      "color2":{"color_type":"custom","colorcode_RGB":[255,255,255]},
      "brightness1":100,
      "brightness2":29,
      "progress":46
    }
  }
}

{
  "update_scene":{"new_params":{"progress":58}}
}

{
  "change_scene":{
    "new_scene":"pulse" ,
    "new_params":{
      "color1":{"color_type":"custom","colorcode_RGB":[255,0,0]},
      "period":900,
      "dutycycle":0.2525252,
      "brightness1":100,
      "brightness2":22
    }
  }
}

{
  "change_scene":{
    "new_scene":"rainbow" ,
    "new_params":{"period":900}
  }
}

{
  "change_scene":{
    "new_scene":"tricolor" ,
    "new_params":{
      "color1":{"color_type":"defaultcolor_1","colorcode_RGB":[255,0,0]},
      "color3":{"color_type":"defaultcolor_3","colorcode_RGB":[]},
      "color2":{"color_type":"defaultcolor_2","colorcode_RGB":[255,255,255]},
      "period":900,
      "brightness1":100
    }
  }
}




{
	"broadcast": {
		"bc_message": "all_off",
		"bc_params": {}
	}
}
{
	"broadcast": {
		"bc_message": "set_iniscn",
		"bc_params": {
		<JSON containing ini file>
		}
	}
}
{
	"broadcast": {
		"bc_message": "set_defaults",
		"bc_params": {
		<JSON containing defaults file>
		}
	}
}
{
	"broadcast": {
		"bc_message": "set_strip",
		"bc_params": {
				"strip":[{
					"num_leds":224,
					"max_current_mA":10000,
					"reverse_outputorder":false,
					"begin_1":0,
					"end_1":225,
					"end_2":224,
					"begin_2":224,
					"correction":{
						"colorcode_RGB":[255,210,170]
					}
				}]
		}
	}
}
{
	"broadcast": {
		"bc_message": "static_rgb",
		"bc_params": {
			"color1":{"color_type":"custom","colorcode_RGB":[123,0,0]}
		}
	}
}
{
	"broadcast": {
		"bc_message": "index",
		"bc_params": {}
	}
}
{
  "broadcast":{
    "bc_message":"ColorCorrect",
    "bc_params":{
      "ColorCorrect":{"color_type":"custom","colorcode_RGB":[123,0,0]}
    }
  }
}