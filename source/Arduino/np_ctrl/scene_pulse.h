// ================================================================================
// ========== light scene PULSE ===================================================

/*
##parameters:##
base brightness [%] - ok
color [R,G,B] - ok
max brightness [%] - ok
period [ms] - ok
dutycylce [p.u.] - ok

##description:##
This light scene should look like a representation of a heartbeat. All LEDs shine with base brightness
and color. Every period the brightness pulses up from brightness1 to brightness2 twice where duty cycle 
defines the part of the period where the two pulses take place. During the other part the brightness 
remains at the base brightness.

##application:##
This light scene could be used to indicate a test bench emergency switch off
*/

class scene_pulse : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period         = 900;
  uint8_t _base_bright  = 75;// internal brightness in 0..255, API in %!  map(BASE_BRIGHT, 0,100,0,255);
  uint8_t _max_bright   = 255;// internal brightness in 0..255, API in %!  map(MAX_BRIGHT, 0,100,0,255);
  float _dutycycle      = 0.3; // part of the periods while light brightness is NOT constant but flashing
  CRGB _rgb             = 0xFF0000;//(40, 160, 110); zuweisung im Kommentar wird in class defintion nicht akzeptiert
  
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max                    = 90;
  uint8_t _bright               = 0;
  uint8_t _base_bright_percent  = 30; // internal brightness in 0..255, API in %!
  uint8_t _max_bright_percent   = 100; // internal brightness in 0..255, API in %! 

  //private JSON deserialization
  bool UpdatePulseJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period               = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max                  = _n->getcntmax();  
    _base_bright_percent    = doc["brightness1"] | _base_bright_percent;
    _base_bright            = map(_base_bright_percent, 0,100,0,255);
    _max_bright_percent     = doc["brightness2"] | _max_bright_percent;
    _max_bright             = map(_max_bright_percent, 0,100,0,255);
    _dutycycle              = doc["dutycycle"] | _dutycycle;
    //Serial.println(_dutycycle);
    const char* _ColorType  = doc["color1"]["color_type"];
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb[i]             = doc["color1"]["colorcode_RGB"][i];
      };
    }
    //Serial.print("nmax = "); Serial.println(_n_max); 
    //Serial.print("1st thres = "); Serial.println(_n_max*(1-_dutycycle)); 
    //Serial.print("2nd thres = "); Serial.println(_n_max*(1-_dutycycle/2)); 
         
    return true;
  }
  
  public:
  //CONSTRUCTOR
  scene_pulse(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "pulse");
    _n = new discreteTimer;
    UpdatePulseJSON(sub_obj);
    Serial.println("creating derived scene scene_pulse");
  }
  virtual ~scene_pulse(){
    delete _n;
    _n = NULL;
    Serial.println("deleting derived scene scene_pulse");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdatePulseJSON(sub_obj);
  }

  //SHOW
  virtual void show_scene(){
    _bright = pulsebright(_n->getcnt(), _n_max, _dutycycle, _base_bright, _max_bright);
    
    for(int i = 0; i < _num_leds; i++){
      _leds_ptr[i] =_rgb;
      _leds_ptr[i].nscale8(_bright);
    }
    FastLED.show();
  }
};
