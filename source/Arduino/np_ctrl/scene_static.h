// ================================================================================
// ========== light scene STATIC ==================================================

/*
## parameters:##
brightness [%] - ok
color [R,G,B] - ok

##description:##
Static light scene, LEDs shine in defined color1 and brightness1. Every now and then, 
a superimposed fade moves through the stripe.

##application:##
nd of idle, test bench online but inactive, waiting for a test
 */

class scene_static : public scene{
  private:
  //specific parameters, remote controllable
  CRGB _rgb_static  = 0xFF0000;
  uint8_t _bright   = 128; // internal brightness in 0..255, API in %! 
   
  //helper variables
  uint8_t _bright_percent  = 50; // internal brightness in 0..255, API in %!
  
  //private JSON deserialization
  bool UpdateStaticJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _bright_percent         = doc["brightness1"] | _bright_percent;
    _bright                 = map(_bright_percent, 0,100,0,255);
    const char* _ColorType  = doc["color1"]["color_type"];
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb_static = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb_static = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb_static = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb_static[i]      = doc["color1"]["colorcode_RGB"][i];
      };
    }    
    return true;
  }
  
  public:
  //CONSTRUCTOR
  scene_static(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "static");
    UpdateStaticJSON(sub_obj);
    Serial.println("creating derived scene scene_static");
  }
  //DESTRUCTOR
  virtual ~scene_static(){
    //delete _n;
    Serial.println("deleting derived scene scene_static");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateStaticJSON(sub_obj);
  }
  //SHOW
  virtual void show_scene(){
    for (int i = 0; i < _num_leds; i++){
      _leds_ptr[i] = _rgb_static;
      _leds_ptr[i].nscale8(_bright);  
    }
    laolafade(0, _num_leds, _num_leds);
    FastLED.show();
  }  
};
