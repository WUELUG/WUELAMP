typedef struct t_defaultcolor{
  CRGB color1;
  CRGB color2;
  CRGB color3;
};

class scene{
  private:
  discreteTimer* _n_fade;
  int _T_period           = 9000;
  float _lalolaDutycycle  = 0.5;
  uint16_t _length        = 50;
  uint8_t _min_bright     = 80;

  protected: 
  uint16_t  _num_leds;
  CRGB*     _leds_ptr;
  char _active_scene[32];
   

  t_defaultcolor *_defaultcolor_ptr; 
  
  // laolaMAXend refers to the max reachable LED within T_Period*DUTYCYCLE, the wave, however stops at laolaend. but with the same speed as if it would stop at laolaMAXend
  void laolafade(uint16_t laolastart,uint16_t laolaend, uint16_t laolaMAXend){
    
    bool fade_active = false;
    bool pos_direction = laolaMAXend>laolastart;
    int i = 0;
    int imax = 0;
    int8_t sign = 1;
    uint16_t fadelead = map(min(_n_fade->getcnt(), _n_fade->getcntmax()*_lalolaDutycycle),0,_n_fade->getcntmax()*_lalolaDutycycle, laolastart, laolaMAXend);
    
    if (pos_direction){ // pos direction
      fade_active = fadelead<laolaend;
      imax = min(_length,(fadelead-laolastart));
      sign = 1;
    }
    else{ // neg direction
      fade_active = fadelead>laolaend;
      imax = min(_length,(laolastart-fadelead));
      sign = -1;
    }
    
    if (fade_active){
      while (i <= imax){
        _leds_ptr[fadelead-(sign*i)].nscale8(_min_bright + map(i,0, imax,0, (255-_min_bright)));
        i++;  
      }
    }
  }
  
  uint8_t pulsebright(int n, int n_max, float dutycycle, uint8_t base_bright, uint8_t max_bright)
  {
    uint8_t bright; 
    if (n < n_max*(1-dutycycle)){//first part of the period: brightness remains constant
      bright = base_bright;
      //Serial.print("base; cnt = "); Serial.println(_n->getcnt());    
    }
    else if (n < n_max*(1-dutycycle/2)){ // second part of the period: two subsequent quasi-sinusoidal waves are added to the base brightness to achieve max brightness
      bright = base_bright + map(quadwave8(map(n,n_max*(1-dutycycle), n_max*(1-dutycycle/2), 0, 255)),0,255,0,(max_bright-base_bright)); // hier eine quad() fktn, die zwei perioden durchläuft, also zwei amplituden auf die base brightnes draufsetzt
      //Serial.print("first; cnt = "); Serial.println(_n->getcnt());     
    }
    else{
      bright = base_bright + map(quadwave8(map(n,n_max*(1-dutycycle/2), n_max, 0, 255)),0,255,0,(0.8*max_bright-base_bright)); // hier eine quad() fktn, die zwei perioden durchläuft, also zwei amplituden auf die base brightnes draufsetzt
      //Serial.print("2nd; cnt = "); Serial.println(_n->getcnt());  
    }
    return bright;
  }
  
  public:
  //CONSTRUCTOR
  scene(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr)
    : _num_leds{num_leds_in},
      _leds_ptr{leds_ptr},
      _defaultcolor_ptr(defaultcolor_ptr)
      
  {
    _n_fade = new discreteTimer;
    _n_fade->setperiod(_T_period);
    Serial.println("creating base scene");
    //_num_leds = sizeof(_leds_ptr)/sizeof(_leds_ptr[0]); // detremination of array size is not possible via a ptr to the array only
  }
  //DESTRUCTOR
  virtual ~scene(){
    delete _n_fade;
    _n_fade = NULL;
    Serial.println("deleting base scene");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){}//not defined in base class
  virtual void set_subscribed_value(int8_t index, float value){}//not defined in base class, see progress
  //GETTER
  virtual char* get_active_scene(){
    return _active_scene;
  }
  //SHOW
  virtual void show_scene(){}//not defined in base class
  //RGB
  virtual void rgb(CRGB *_rgb){
    FastLED.showColor(*_rgb,255);
  }
  //ALL OFF
  virtual void off(){
    for (uint16_t i = 0; i < _num_leds; i++){
      _leds_ptr[i] = CRGB::Black;
      FastLED.show(); 
    }
  }
  // MAP W OVERFLOW: like regular arduino map-function but optimized for circular values that shall increase monotonic even if to_low > to_high.
  // example: you want to fade a hue value from blue ( e.g. h = 240 ) to red (e.g. 10) and use the way crossing 0 instead of decrease the hue and reach the target via green
  uint8_t map_w_overflow(uint16_t in, uint16_t from_low, uint16_t from_high, uint16_t to_low, uint16_t to_high, uint16_t to_overflow){
    uint8_t out = 0;
    uint16_t from_overflow = 0; //tbd
    if(to_low < to_high){ // usual mapping w/o overflow management
      out = map(in, from_low, from_high, to_low, to_high);
      //Serial.println("normal");
    }
    else{ // map using circular overflow
      from_overflow = (float)(to_overflow-to_low)/(to_overflow-to_low+to_high)*(from_high-from_low);
      //Serial.print("from_overflow=");Serial.println(from_overflow);
      if(in<from_overflow){
        out = map(in, from_low, from_overflow, to_low, to_overflow);
        //Serial.println("uf");
      }
      else{
        out = map(in, from_overflow, from_high, 0, to_high);
        //Serial.println("of");
      }
    }
    return out;
  }
  //SHOW INDEX
  void showindex(uint16_t index_increment){
    // helps to count the indices of particular LEDs 
    for(uint16_t i = 0; i < _num_leds; i++){
      _leds_ptr[i] = CRGB::Black; // turn off all LEDs for init
    }
    uint16_t divider = (_num_leds)/index_increment;
    for(uint16_t i = 0; i <= divider; i++){
      if((i*index_increment)%100 == 0){
        _leds_ptr[i*index_increment] = CRGB::Red; // turn red multiples of 100 incl 0
      }
      else{
        _leds_ptr[i*index_increment] = CRGB::White; // turn white multiples of 25 excl multiples of 100
      }      
    }
    uint16_t mid_index = floor(_num_leds/2);
    if(_num_leds%2 == 0){//_num_leds even
      _leds_ptr[mid_index] = CRGB::Green;
      _leds_ptr[mid_index-1] = CRGB::Green;
    }
    else{//_num_leds odd
      _leds_ptr[mid_index] = CRGB::Green;
    }
    FastLED.show();
  }
 
};
