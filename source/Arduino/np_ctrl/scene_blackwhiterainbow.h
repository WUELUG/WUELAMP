/*
## parameters:##
no parameters require

##description:##
circles a combination of black and white sequence and a special rainbow

##application:##
Corporate identity
 */
class scene_blackwhiterainbow : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period     = 30000;
   
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max; 
  uint16_t _startpointer = 0; // _startpointer iterates the position of the first rainbowcolor periodically though the stripe

  CHSV* _lut; //dynamically allocated array containing a lookuptable with the rainbow colors
  CHSV rainbow_green ;//RGB:(161, 198, 50)
  CHSV rainbow_torquiose;//RGB:0, 153, 147)
  CHSV rainbow_purple; //RGB:(53, 42, 132)
  CHSV rainbow_pink;//RGB:(164, 35, 132)
  CHSV rainbow_red; //RGB:(212,1,21)
  CHSV rainbow_yellow; //RGB:(255, 220,0)
  CHSV rainbow[6];

  // not required:
  CRGB rainbow_green_r     = 0xA1C632; //RGB:(161,198,50)  HSV: 75°, 74,7%, 77,6%%   HSV in 0...255: 53, 190, 198
  CRGB rainbow_torquiose_r = 0x009993; //RGB:0,153,147)    HSV: 178°, 100%, 60%      HSV in 0...255: 126, 255, 153
  CRGB rainbow_purple_r    = 0x352A84; //RGB:(53,42,132)   HSV: 247°, 68,2%, 51,8%   HSV in 0...255: 175, 174, 132
  CRGB rainbow_pink_r      = 0xA42384; //RGB:(164,35,132)  HSV: 315°, 78,7%, 64,3%   HSV in 0...255: 223, 201, 164
  CRGB rainbow_red_r       = 0xD40115; //RGB:(212,1,21)    HSV: 354°, 99,5%, 83,1%   HSV in 0...255: 251, 254, 212
  CRGB rainbow_yellow_r    = 0xFFDC00; //RGB:(255,220,0)   HSV: 52°, 100%, 100%      HSV in 0...255: 37, 255, 255

  //private JSON deserialization
  bool UpdateBWRainbowJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period               = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max                  = _n->getcntmax();  
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
    
  public:
  //CONSTRUCTOR
  scene_blackwhiterainbow(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "bwrainbow");
    _n = new discreteTimer;
    UpdateBWRainbowJSON(sub_obj);
    _lut      = new CHSV[_num_leds];
    
    rainbow_green.setHSV(53, 190, 198);
    rainbow_torquiose.setHSV(126, 255, 153);
    rainbow_purple.setHSV(175, 174, 132);
    rainbow_pink.setHSV(223, 201, 164);
    rainbow_red.setHSV(251, 254, 212);
    rainbow_yellow.setHSV(37, 255, 255);
    rainbow[0] = rainbow_green;
    rainbow[1] = rainbow_torquiose;
    rainbow[2] = rainbow_purple;
    rainbow[3] = rainbow_pink;
    rainbow[4] = rainbow_red;
    rainbow[5] = rainbow_yellow;
    
    uint16_t startbw = 0;
    uint16_t endbw = _num_leds/2;
    //startbw = _num_leds/2;
    //endbw = _num_leds;
    uint8_t nrOfBWs = 8;
    uint16_t nrOfBWLEDS = endbw-startbw;
    
    for(uint16_t i = startbw; i < endbw; i++){
      if(i%(nrOfBWLEDS/nrOfBWs)<(nrOfBWLEDS/nrOfBWs/2)){
        _lut[i].setHSV(0,0,0); //black
      }
      else{
        _lut[i].setHSV(0,0,255); // white
      }
    }
    uint16_t startrb = _num_leds/2;
    uint16_t endrb = _num_leds;
    //startrb = 0;
    //endrb = _num_leds/2;

    for(uint16_t i = startrb; i < endrb; i++){
      uint8_t nrOfColors = 6;
      uint16_t nrOfRBLEDS = endrb-startrb;
      //Serial.print("i = "); Serial.println(i);
      uint16_t index = (i-startrb)%(nrOfRBLEDS/(nrOfColors-1));
      //Serial.print("index = "); Serial.println(index);
      uint8_t colorindex = floor((i-startrb)/(nrOfRBLEDS/(nrOfColors-1)));
      //Serial.print("colorindex = "); Serial.println(colorindex);
      _lut[i].h = map_w_overflow(index, 0, nrOfRBLEDS/(nrOfColors-1), rainbow[colorindex].h, rainbow[colorindex+1].h, 255);
      _lut[i].s = map(index, 0, nrOfRBLEDS/(nrOfColors-1), rainbow[colorindex].s, rainbow[colorindex+1].s);
      _lut[i].v = map(index, 0, nrOfRBLEDS/(nrOfColors-1), rainbow[colorindex].v, rainbow[colorindex+1].v);

    }
    Serial.println("creating derived scene scene_blackwhiterainbow");
  }
  //DESTRUCTOR
  virtual ~scene_blackwhiterainbow(){
    delete _n;
    _n = NULL;
    delete _lut;
    _lut = NULL;
    Serial.println("deleting derived scene scene_blackwhiterainbow");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateBWRainbowJSON(sub_obj);
  }
  //SHOW
  virtual void show_scene(){
    _startpointer = map(_n->getcnt(),0,_n_max,0,(_num_leds-1));
    for(uint16_t i = 0; i < _num_leds; i++){
      _leds_ptr[(_startpointer+i)%(_num_leds-1)] = _lut[i];
    }
    FastLED.show();
  }
};
