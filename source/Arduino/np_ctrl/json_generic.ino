// ================================================================================
// ========== generic JSON pasring ================================================

// this codevparses the content of a received command and returns them in a format the calling function can handle

// resets the payload analysis type
void ResetPayloadStruct(t_Payload_Analysis *_result) {
  _result->is_broadcast = false;
  _result->is_scene_change = false;
  _result->is_scene_update = false;
  _result->is_not_valid = false;
}

// determines the payload type and slices the JSON object
void CheckJSONPayload(char *payload, t_Payload_Analysis *_result, char *_newcmd, char *_newparms) {

  ResetPayloadStruct(_result);
  
  StaticJsonDocument<1024> doc;
  DeserializationError error = deserializeJson(doc, payload);

  if (error) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());

    // Abbruch 
    _result->is_not_valid = true;
    return;
  }

  JsonObject obj = doc.as<JsonObject>();

  for (JsonPair p : obj) {
    if (p.key() == "broadcast") {
      Serial.println("Broadcast detected!");
      const char* _bc_msg = doc["broadcast"]["bc_message"];
      if (_bc_msg != nullptr) {
        _result->is_broadcast = true;
        strlcpy(_newcmd, _bc_msg, 32);
        serializeJson(obj["broadcast"]["bc_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
        return;  
      }
      else {
        Serial.println("no 'bc_message' key found!");
        _result->is_not_valid = true;
        return;
      }
    }
    if (p.key() == "change_scene") {
      Serial.println("Scene Change Request detected!");
      const char* _sc_msg = doc["change_scene"]["new_scene"];
      if (_sc_msg != nullptr) {
        _result->is_scene_change = true;
        strlcpy(_newcmd, _sc_msg, 32);
        serializeJson(obj["change_scene"]["new_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
        return;  
      }
      else {
        Serial.println("no 'new_scene' key found!");
        _result->is_not_valid = true;
        return;
      }
    }
    if (p.key() == "update_scene") {
      Serial.println("Scene Update detected!");
      _result->is_scene_update = true;
      serializeJson(obj["update_scene"]["new_params"], _newparms, 1024);  // Sub-Objekt wieder als JSON zurückgeben
      return;  
     }
   }
}
