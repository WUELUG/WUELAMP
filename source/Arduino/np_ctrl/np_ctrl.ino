
// ================================================================================
// ========== WUELAMP =============================================================
// ========== WUELUG LabVIEW Arduino MultiLED Pixel ===============================
// ================================================================================


// ================================================================================
// ========== includes ============================================================
#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <FastLED.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <SD.h>

#include "discreteTimer.h"
#include "scene_baseclass.h"
#include "scene_twinkle.h"
#include "scene_loading.h"
#include "scene_pulse.h"
#include "scene_progress.h"
#include "scene_rainbowwheel.h"
#include "scene_static.h"
#include "scene_tricolorwheel.h"
#include "scene_blackwhiterainbow.h"

// ================================================================================
// ========== typedefs ============================================================
typedef struct t_Payload_Analysis {
  bool is_broadcast;
  bool is_scene_change;
  bool is_scene_update;
  bool is_not_valid;
};
typedef struct t_network_config {
  char client_type[32];
  char mac[18];
  bool use_dhcp;
  char ip[16]; 
  char subnet[16];
  char gateway[16];
  char dns[16];  
};
typedef struct t_mqtt_config{
  char protocol_version[8];
  char broker_name[32];
  char broker_ip[16];
  char client_id[32];
  char user[32];
  char password[32];
  uint8_t numberOfTopics;
  char **listen_on;//[5][127];
};
typedef struct t_strip_config{
  //char strip_type[16]; // template parameter, must be fix at compile time 
  uint16_t num_leds;
  //uint8_t data_pin; // template parameter, must be fix at compile time 
  //char color_order[4]; // template parameter, must be fix at compile time 
  uint16_t max_current_mA;
  bool reverse_outputorder;
  CRGB correction;
};
// ================================================================================
// ========== defines =============================================================
#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif
// hardware specific settings
#define ENABLE_LEVELSHIFT 6
#define DATA_PIN 0
// LED-Controller-specific settings
#define LED_TYPE   WS2812
#define COLOR_ORDER   GRB
// ================================================================================
// ========== global definitions and declarations =================================
EthernetClient      ethClient;
PubSubClient        client(ethClient);

t_defaultcolor      defaultcolor;   // definition cf scene_baseclass.h
t_progress_init     progress_init;  // definition cf scene_progress.h
t_Payload_Analysis  Payload_Analysis;
t_mqtt_config       mqtt;
t_strip_config      strip;

char NewCommand[32]     = "";   // Achtung: Limitiert die möglichen Längen für Broadcast- und Szenen-Namen!!!
char NewParams[1024]    = "";   // Und auch das New-Params-Objekt ist limitiert. Der Einfachheit halber wie das ursprüngliche JSON-Dokument
// ToDo: Diese Max-Längen als Defines ganz oben festlegen!

// Define the array of leds
CRGB *leds;

// create scene objects
scene* scene_ptr;// base-class pointer

//discreteTimer* n_main;
//uint8_t numberOfScenes;

// ================================================================================
// ========== setup ===============================================================
void setup() {
  // ==============================================================================
  // ========== open serial port ==================================================
  delay(3000);
  Serial.begin(9600);
  char versionString[]= "v1.4.0";
  Serial.print("\nsetting up WUELAMP ");Serial.println(versionString);
  // ==============================================================================
  // ========== load config =======================================================
  t_network_config network;
  byte localip[4];
  byte serverip[4];
  byte mac[6];  
  readConfigfromSD(&network); // network is the only local struct, oter strucs are global
  // ==============================================================================
  // ========== initialize Ethernet and MQTT ======================================
  parseIP(network.ip, localip);
  parseIP(mqtt.broker_ip, serverip);
  parseMAC(network.mac, mac);
  client.setServer(serverip, 1883);
  client.setCallback(callback);
  client.setBufferSize(512);
  Ethernet.begin(mac, localip);
  // ==============================================================================
  // ========== enable levelshifter on connector pcb ==============================
  pinMode(ENABLE_LEVELSHIFT, OUTPUT);
  digitalWrite(ENABLE_LEVELSHIFT, HIGH);
  // initialize LED stripe
  leds = new CRGB[strip.num_leds];
  LEDS.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds,strip.num_leds).setCorrection(strip.correction);
  //LEDS.setBrightness(255); // generally limit max brightness for all LEDs
  FastLED.setMaxPowerInVoltsAndMilliamps(5,strip.max_current_mA); //limit the maximum power/current consumption for the stripe
  FastLED.clear();
  // ==============================================================================
  // ========== load startup scene ================================================
  analysePayload(); // initial Payload was read from SD card, here scene_ptr must be set

}
unsigned long t_0 = 0;
unsigned long t_1 = 0;
unsigned long t_2 = 0;
unsigned long t_3 = 0;
unsigned long t_last = 0;

// ================================================================================
// ========== main loop ===========================================================
void loop()
{
  t_0 = millis();
  if (!client.connected()) {
    reconnect();
  }
  t_1 = millis();
  client.loop();
  t_2 = millis();
  scene_ptr->show_scene();
  t_3 = millis();
  //Serial.print("con-check: ");Serial.println(t_1-t_0);
  //Serial.print("mqtt client: ");Serial.println(t_2-t_1);
  //Serial.print("show scene: ");Serial.println(t_3-t_2);
  //Serial.print("total: ");Serial.println(t_0-t_last);
  t_last = t_0;
  
}
