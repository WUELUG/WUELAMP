/*
## parameters:##


##description:##

##application:##
 */
class scene_loading : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period     = 10000;
   
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max; 
  
  //private JSON deserialization
  bool UpdateProgressJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max  = _n->getcntmax();    
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
 
  public:
  //CONSTRUCTOR
  scene_loading(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "template");
    UpdateProgressJSON(sub_obj);
    _n = new discreteTimer;
    _n->setperiod(_T_period); //möglicherweise einen globalen zähler instanziieren und prt darauf an die aktive szene übergeben
    _n_max    = _n->getcntmax();
    Serial.println("creating derived scene scene_loading");
  }
  //DESTRUCTOR
  virtual ~scene_loading(){
    delete _n;
    _n = NULL;
    Serial.println("deleting derived scene scene_loading");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateProgressJSON(sub_obj);
  }
  //SHOW
  virtual void show_scene(){
  }  
};
