// ================================================================================
// ========== light scene PROGRESS ================================================

/*
##parameters:##
brightness elapsed [%] - ok
color elapsed [R,G,B] - ok
brightness remaining [%] - ok
color remaining [R,G,B] - ok
direction [0,1] - automatically via 
progress [%] - ok

##description:##
This light scene represents a progress bar. the elapsed ("filled") part of the bar shines in brightness2 
and color2, the other part in brightness1 and color1.
As there are different ways to install the LED stipe, the direction needs to be a bit more flexible:
Either the stripe is installed in a linear way (e.g. under the table, direction = 0). Here, the progress 
bar starts at begin1 (LED indices defined in stipe.txt) and ends at end1. Or the stripe is installed in 
a circular way (e.g. in a rack, direction = 1) Here, two progress start from begin1 and begin2 in parallel 
and end at end1 or end2, respectively. Every now and then, a superimposed fade moves through the elapsed 
part of the stripe.

##application:##
e.g. test program progress
*/


typedef struct t_progress_init{
  uint16_t Begin1;
  uint16_t End1;
  uint16_t End2;
  uint16_t Begin2;
  bool demo;
  float lower_limit;
  float upper_limit;
  char calc_mode[8];
  uint8_t numberOfTopics;
  char **listen_on;//[5][127];
};

class scene_progress : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period                 = 15000;
  uint8_t _direction_progress   =  1; // 0: linear (table) 1: circular (rack) // promote as remote-tunable parameter
  //alternativ als enum:
  //enum t_SceneParams_ProgressDirection {
   // linear,
   // bidirectional
  //};
  uint16_t _progr_in    = 0;
  CRGB _rgb_fill        = 0x00FF00;
  CRGB _rgb_base        = 0xFF0000;
  CHSV _hsv_fill;
  uint8_t _bright_fill  = 128; // internal brightness in 0..255, API in %!
  uint8_t _bright_base  = 128; // internal brightness in 0..255, API in %! 
   
  //helper variables
  uint16_t _progr_map1  = 0;
  uint16_t _progr_map2  = 0;
  uint16_t _progr_out   = 0;
  discreteTimer* _n       = NULL; // create timer object
  discreteTimer* _n_pulse = NULL; 
  int _n_max; 
  int _n_pulse_max; 
  uint8_t _bright_fill_percent  = 50; // internal brightness in 0..255, API in %!
  uint8_t _bright_base_percent  = 50; // internal brightness in 0..255, API in %!
  float *_subscribed_values; 

  //init variables
  // define special LED positions, e.g. corners of the rack
  uint16_t _Begin1        = 5;
  uint16_t _End1          = 30;
  uint16_t _End2          = 35;
  uint16_t _Begin2        = 70;
  bool _demo              = false; 
  float _lower_limit      = 0;
  float _upper_limit      = 0;
  uint8_t _numberOfTopics = 0;
  char _calc_mode[8];
  
  //private JSON deserialization
  bool UpdateProgressJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _progr_in               = doc["progress"] | _progr_in;    // Ursprünglichen Wert hinter | heißt: verwende den als default, wenn ["progress"] nicht gefunden 
    _direction_progress     = doc["direction"] | _direction_progress;
    const char* _ColorType  = doc["color1"]["color_type"]; // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb_base = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb_base = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb_base = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb_base[i]        = doc["color1"]["colorcode_RGB"][i] | _rgb_base[i];
      };
    }
    _bright_base_percent    = doc["brightness1"] | _bright_base_percent;
    _bright_base            = map(_bright_base_percent, 0,100,0,255);
    _ColorType              = doc["color2"]["color_type"];
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb_fill = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb_fill = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb_fill = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb_fill[i]        = doc["color2"]["colorcode_RGB"][i] | _rgb_fill[i];
      };
    }
    _bright_fill_percent    = doc["brightness2"] | _bright_fill_percent;
    _bright_fill            = map(_bright_fill_percent, 0,100,0,255);
    //if (doc["brightness2"] != NULL){// funktioniert nicht, ist immer NULL
    //  _bright_fill  = map(doc["brightness2"], 0,100,0,255);
    // }
    //Serial.print("docbright: "); Serial.println(doc["brightness2"]);
    /*
    Serial.print("_rgb_base=");Serial.print(_rgb_base.r);Serial.print(_rgb_base.g);Serial.println(_rgb_base.b);
    Serial.print("_rgb_fill=");Serial.print(_rgb_fill.r);Serial.print(_rgb_fill.g);Serial.println(_rgb_fill.b);
    Serial.print("_bright_base=");Serial.println(_bright_base);
    Serial.print("_bright_fill=");Serial.println(_bright_fill);
    Serial.print("_progr_in=");Serial.println(_progr_in);
    Serial.print("_direction_progress=");Serial.println(_direction_progress);*/
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde
  }
  float get_processed_subscription_value(){
    if (_numberOfTopics == 0){
      return 0;
    }
    float result = _subscribed_values[0];
    if (strcmp(_calc_mode, "min") == 0){
      for (uint8_t i = 1; i< _numberOfTopics; i++){
        if (_subscribed_values[i] < result){
          result = _subscribed_values[i];
        }
      }
    }
    else if (strcmp(_calc_mode, "max") == 0){
      for (uint8_t i = 1; i< _numberOfTopics; i++){
        if (_subscribed_values[i] > result){
          result = _subscribed_values[i];
        }
      }
    }
    else if (strcmp(_calc_mode, "mean") == 0){
      for (uint8_t i = 1; i< _numberOfTopics; i++){
          result += _subscribed_values[i];
      }
      result = result / _numberOfTopics;
    }
    else if (strcmp(_calc_mode, "sum") == 0){
      for (uint8_t i = 1; i< _numberOfTopics; i++){
          result += _subscribed_values[i];
      }
    }
    //Serial.print("processed value="); Serial.println(result);
    return result; 
  } 

 
  public:
  //CONSTRUCTOR
  scene_progress(uint16_t _num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj, t_progress_init *init_ptr)
    : scene{_num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "progress");
    _Begin1   = init_ptr->Begin1;
    _End1     = init_ptr->End1;
    _End2     = init_ptr->End2;
    _Begin2   = init_ptr->Begin2;
    _demo     = init_ptr->demo;
    _lower_limit    = init_ptr->lower_limit;
    _upper_limit    = init_ptr->upper_limit;
    _numberOfTopics = init_ptr->numberOfTopics; 
    strcpy(_calc_mode, init_ptr->calc_mode);
    
    if(_Begin2 > _num_leds){ //configuration fault, the following code doesn't return senseful results, but prevents unallowed states
      _Begin1 = (float)_Begin1/_Begin2*_num_leds;
      _End1 = (float)_End1/_Begin2*_num_leds;
      _End2 = (float)_End2/_Begin2*_num_leds;
      _Begin2 = (float)_Begin2/_Begin2*_num_leds;
    }
    UpdateProgressJSON(sub_obj);
    //initialize timer for demo mode:
    if (_demo == 1){
      _n = new discreteTimer;
      _n->setperiod(_T_period); //möglicherweise einen globalen zähler instanziieren und prt darauf an die aktive szene übergeben
      _n_max    = _n->getcntmax();
    }
    _n_pulse = new discreteTimer;
    _n_pulse->setperiod(900); //möglicherweise einen globalen zähler instanziieren und prt darauf an die aktive szene übergeben
    _n_pulse_max    = _n_pulse->getcntmax();

    _subscribed_values  = new float[_numberOfTopics];
    for (uint8_t i = 0; i < _numberOfTopics; i++){
      _subscribed_values[i] = 0;
    }
    _hsv_fill.setHSV(0,255,255);
    Serial.println("creating derived scene scene_progress");
  }
  //DESTRUCTOR
  virtual ~scene_progress(){
    if (_n != NULL){
      delete _n;
    }
    _n = NULL;
    if (_subscribed_values != NULL){
      delete _subscribed_values;
    }
    Serial.println("deleting derived scene scene_progress");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateProgressJSON(sub_obj);
  }
  virtual void set_subscribed_value(int8_t index, float value){
    _subscribed_values[index] = value;
  }
  //SHOW
  virtual void show_scene(){
    // DRAFT, DOESNT MEET THE REQUIREMENTS, YET
    //Serial.println(_progr_in);
    if (_demo == 1){
      _progr_in = map(_n->getcnt(),0, (_n->getcntmax()-1), 0, 100);
      //Serial.println(_progr_in);
    }
    uint16_t _Begin2_ =  _Begin2;
    if(_Begin2 >= _num_leds-1){
      uint16_t _Begin2_ =  _num_leds-2;
    }
    //to decouple the incoming percentage from the displayed percentage the following code maps progr_in to progr_out
    if (_progr_in < 109)
    {
      _progr_out = constrain(_progr_in,0,100);
    }
    else
    {
      //_progr_in == 110
      // some special effects nobody has expected in this function
      // 1) somevalue is mapped to the progress bar progress, still using the predefined colors
      _progr_out = map(get_processed_subscription_value(), _lower_limit, _upper_limit, 10, 100);
      _progr_out = constrain(_progr_out, 10, 100);
      //Serial.println(somevalue);
      //Serial.println(_progr_out);
      if (_progr_in >= 120)
      {
        // 2) additionally, somevalue is mapped to the color hue 
        //_hsv_fill.s and _hsv_fill.v were set to 255 in the constructor
        _hsv_fill.h = map(_progr_out, 20, 100, 89, 0);
        _hsv_fill.h = constrain(_hsv_fill.h, 0,89);
        if (_progr_in >= 130 && _progr_out > 80)
        {
          // 3) additionally, the color value (brightness) starts pulsing when somevalue exceeds a threshold
          _hsv_fill.v = pulsebright(_n_pulse->getcnt(), _n_pulse_max, 0.3, map(_progr_out, 80, 100, 255, 80), 255);
        }
        else
        {
          _hsv_fill.v = 255;
        }
        hsv2rgb_rainbow(_hsv_fill,_rgb_fill); 
          _rgb_base        = 0x000000;
      }
      
    }
    
    //currently only circular rack installation. for other directions (table): code needed
    _progr_map1 = map(_progr_out, 0, 100, 0 , _End1-_Begin1);
    _progr_map2 = map(_progr_out, 0, 100, 0, _Begin2-_End2); // 
    uint16_t i                  = 0;
    for (i = 0; i < _Begin1; i++){
      _leds_ptr[i] = CRGB::Black; 
    }
    for (i = _Begin1; i < _Begin1 + _progr_map1; i++){
      _leds_ptr[i] = _rgb_fill;
      _leds_ptr[i].nscale8(_bright_fill); 
    }
    for (i = _Begin1 + _progr_map1; i < _End1; i++){
      _leds_ptr[i] = _rgb_base;
      _leds_ptr[i].nscale8(_bright_base); 
    }
    laolafade(_Begin1, _Begin1 + _progr_map1, _End1);
    if (_direction_progress == 1){
      for (i = _End1; i <= _End2; i++){
        _leds_ptr[i] = CRGB::Black; 
      }
      for (i = _End2+1; i <= _Begin2_-_progr_map2; i++){
        _leds_ptr[i] = _rgb_base;
        _leds_ptr[i].nscale8(_bright_base); 
      }
      for (i = (_Begin2_ - _progr_map2 + 1); i <= _Begin2_; i++){
        _leds_ptr[i] = _rgb_fill;
        _leds_ptr[i].nscale8(_bright_fill); 
      }
      for (i = _Begin2_+1; i < _num_leds; i++){
        _leds_ptr[i] = CRGB::Black; 
      }
      laolafade(_Begin2_, _Begin2_-_progr_map2, _End2);
    }
    FastLED.show();
  }  
};
