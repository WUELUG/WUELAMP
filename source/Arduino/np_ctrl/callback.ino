// ================================================================================
// ========== MQTT message handler ================================================

// this code manages the MQTT connection and defines what happens if a message 
// is received on one of the topics, this device is subsribed to. 

long t_lastreconnect = -5000;


// ================================================================================
// ========== mreconnect ==========================================================
void reconnect() {
  // try to connect every 5 s
  if(millis()-t_lastreconnect > 5000){
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqtt.client_id, mqtt.user, mqtt.password)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic","hello world");
      // ... and resubscribe
      for(int i = 0; i<mqtt.numberOfTopics; i++){
          client.subscribe(mqtt.listen_on[i]);
          Serial.println(mqtt.listen_on[i]);
      }
      for(int i = 0; i<progress_init.numberOfTopics; i++){
          client.subscribe(progress_init.listen_on[i]);
          Serial.println(progress_init.listen_on[i]);
      }
      client.publish("subscribed",mqtt.client_id);
      Serial.print("subscribed"); 
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
    }
    t_lastreconnect = millis();
  }
}

// ================================================================================
// ========== callback ============================================================

//automatocally called by the pubsubclient lib on receiving messages

// wenn topic element aus der listen_on liste bzw. höchste hierarchie daraus, mache das, was bisher auch gemacht wurde. wenn topic aus der subscribetoprogress liste, verrechne die oder den parameter und mach damit weiter
// dann gibt es eine public methode von progress, mit der ein oder mehrere topics in den progress objekt geschrieben werden können. außerdem gibt es eine private porgress methode, die je nach Konfiguration einen analogwert, den mittwlwert der 
// subscription werte, das min, das max, das rms zurückgibt. dieser rückgabewert wird dann als "somevalue" verwendet. die arraygröße von den intern gehaltenen subscription werten ergibt sich aus der anzahl der topics in der config datei. 
// das array muss zur laufzeit im constructor mit new instanziiert werdn und im destruktor wieder freigegebenw werden
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] \n");
  char _payload[length];
  for (int i=0;i<length;i++) {
    _payload[i] = (char)payload[i];
    Serial.print(_payload[i]);    
  }
  int8_t _indexprogress = findindex(topic, progress_init.listen_on, progress_init.numberOfTopics);
  if (findindex(topic, mqtt.listen_on, mqtt.numberOfTopics)>=0){
    CheckJSONPayload(_payload, &Payload_Analysis, NewCommand, NewParams);
    analysePayload();
  }
  else if (_indexprogress >= 0 && (strcmp(scene_ptr->get_active_scene(), "progress") == 0)) // only if there are progress topics and scene progress is active
  {
    //Serial.println(_payload);
    float val = atof(_payload);
    //Serial.print("val atof =");Serial.println(val);
    scene_ptr->set_subscribed_value(_indexprogress, val);
  }
  else
  {
    //do nothing
  }

}

//separat testen // vielleicht auch int8_t
int8_t findindex(char *topic, char **listen_on, uint8_t numberOfTopics)
{
  //Serial.println(topic);
  for(uint8_t i = 0; i < numberOfTopics; i++){
    if (strcmp(topic, listen_on[i]) == 0) {
      //Serial.println(listen_on[i]);
      //Serial.println(i);
      return i;
    }
  }
  return -1; 
}

// ================================================================================
// ========== analyse payload and execute commands ================================
void analysePayload(){
    if (Payload_Analysis.is_broadcast) {
    //Serial.print("Broadcast received in caller. ");

    if (strcmp(NewCommand, "reset") == 0) {                      
      //Serial.println("It is a reset broadcast!");
    }
    if (strcmp(NewCommand, "set_iniscn") == 0) {   
      //Overwrites the content of iniscn.txt on the SD Card with the value of the "bc_params"-key. The value must be a valid JSON string without the top level "{}" which are added whild wirting to the SD Card.                   
      //Serial.println("It is a set_iniscn broadcast!");
        rewriteConfOnSD(NewParams,"iniscn.txt");
    }
    if (strcmp(NewCommand, "set_defaults") == 0) {  
      //Overwrites the content of defaults.txt on the SD Card with the value of the "bc_params"-key. The value must be a valid JSON string without the top level "{}" which are added whild wirting to the SD Card.                    
      //Serial.println("It is a set_iniscn broadcast!");
        rewriteConfOnSD(NewParams,"defaults.txt");
    }
    if (strcmp(NewCommand, "set_strip") == 0) {  
      //Overwrites the content of strip.txt on the SD Card with the value of the "bc_params"-key. The value must be a valid JSON string without the top level "{}" which are added whild wirting to the SD Card.                    
      // Serial.println("It is a set_iniscn broadcast!");
        rewriteConfOnSD(NewParams,"strip.txt");
    }
    if (strcmp(NewCommand, "set_progress") == 0) {  
      //Overwrites the content of strip.txt on the SD Card with the value of the "bc_params"-key. The value must be a valid JSON string without the top level "{}" which are added whild wirting to the SD Card.                    
      // Serial.println("It is a set_iniscn broadcast!");
        rewriteConfOnSD(NewParams,"progress.txt");
    }
    if (strcmp(NewCommand, "ColorCorrect") == 0) { 
      // Applies new color correction parameters for testing and finding the proper parameters. Does not store them permantently on SD card. Once found, proper parameters must be set in strip.txt
      CRGB _rgb_corr  = 0xFFFFFF;                     
      DynamicJsonDocument doc(512);
      DeserializationError error = deserializeJson(doc, NewParams);
      if (error) {
        Serial.print(F("deserializeJson() failed with code "));
        Serial.println(error.c_str());
        // Abbruch 
        return;
      }
      const char* _ColorType  = doc["ColorCorrect"]["color_type"];
      if (strcmp(_ColorType, "custom") == 0){
        for (int i = 0; i <= 2; i++) {
          _rgb_corr[i]      = doc["ColorCorrect"]["colorcode_RGB"][i];
        }
      }
      FastLED.setCorrection(CRGB(_rgb_corr[0],_rgb_corr[1],_rgb_corr[2]));   
    }
    if (strcmp(NewCommand, "static_rgb") == 0) {
      // Shows static rgb color on all LEDs of the stripe
      CRGB      _rgb          = 0xFFFFFF;                     
      DynamicJsonDocument doc(512);
      DeserializationError error = deserializeJson(doc, NewParams);
      if (error) {
        Serial.print(F("base deserializeJson() failed with code "));
        Serial.println(error.c_str());
        // Abbruch 
        return;
      }
      const char* _ColorType  = doc["color1"]["color_type"];
      if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb = defaultcolor.color1;
      if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb = defaultcolor.color2;
      if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb = defaultcolor.color3;
      if (strcmp(_ColorType, "custom") == 0){
        for (int i = 0; i <= 2; i++) {
          _rgb[i]      = doc["color1"]["colorcode_RGB"][i];
        }
      }    
      delete scene_ptr;             
      scene_ptr = new scene(strip.num_leds, leds, &defaultcolor);
      scene_ptr->rgb(&_rgb);  
    }
    if (strcmp(NewCommand, "all_off") == 0) {
      //Turns off all LEDs of the stripe
      delete scene_ptr;
      scene_ptr = new scene(strip.num_leds, leds, &defaultcolor); //prevent child class to sho wher scene on main loop call "show_scene()"
      scene_ptr->off(); 
      //Serial.println("It is an all-off broadcast!");
    }
    if (strcmp(NewCommand, "index") == 0) { 
      //helps to count the indices of particular LEDs and to find proper settings for strip.txt
      delete scene_ptr;                     
      scene_ptr = new scene(strip.num_leds, leds, &defaultcolor);
      scene_ptr->showindex(10);
    }
    
  }

  if (Payload_Analysis.is_scene_change) {
    delete scene_ptr;
    if (strcmp(NewCommand, "loading") == 0) {
      scene_ptr = new scene_loading(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "progress") == 0) {
      scene_ptr = new scene_progress(strip.num_leds, leds, &defaultcolor, NewParams, &progress_init);
    }
    else if (strcmp(NewCommand, "pulse") == 0) {
      scene_ptr = new scene_pulse(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "rainbow") == 0) {
      scene_ptr = new scene_rainbowwheel(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "static") == 0) {
      scene_ptr = new scene_static(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "tricolor") == 0) {
      scene_ptr = new scene_tricolorwheel(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "twinkle") == 0) {
      Serial.println(NewParams);
      scene_ptr = new scene_twinkle(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else if (strcmp(NewCommand, "bwrainbow") == 0) {
      scene_ptr = new scene_blackwhiterainbow(strip.num_leds, leds, &defaultcolor, NewParams);
    }
    else{
      scene_ptr = new scene(strip.num_leds, leds, &defaultcolor);
      scene_ptr->showindex(10);
    }
  }
  
  if (Payload_Analysis.is_scene_update) {
    scene_ptr->update_scene(NewParams);
    //Serial.print("New parameters: ");
    //Serial.println(NewParams);
  }
}
