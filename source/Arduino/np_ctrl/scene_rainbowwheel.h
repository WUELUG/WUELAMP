// ================================================================================
// ========== light scene RAINBOWWHEEL ============================================

/*
## parameters:##
period [ms]
brightness [%]

##description:##
This scene creates a rainbow starting with red on the first LED and finishing 
with purple on the last led. Over the time, the index of the first rainbow color 
(red, first led at the beginning) rotates slowly (as defines by period) through 
the stripe and makes the rainbow move.

##application:##
no idea, but looks impressive. could be an idle or demo state
 */
class scene_rainbowwheel : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period   = 10000;
  uint8_t _bright = 255;
   
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max;
  uint8_t _bright_percent = 100; 
  CHSV _hsv;
  uint16_t _k   = 0;
  uint16_t _m   = 0;
  uint8_t _hue  = 0;

  //private JSON deserialization
  bool UpdateRainbowJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period       = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max          = _n->getcntmax();   
    _bright_percent = doc["brightness1"] | _bright_percent;
    _bright         = map(_bright_percent, 0,100,0,255);
   
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
  
  public:
  //CONSTRUCTOR
  scene_rainbowwheel(uint16_t _num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{_num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "rainbow");
    _n = new discreteTimer;
    UpdateRainbowJSON(sub_obj);
    _n->setperiod(_T_period); //möglicherweise einen globalen zähler instanziieren und prt darauf an die aktive szene übergeben
    _n_max    = _n->getcntmax();
    _hsv.setHSV(0,255,255);
    Serial.println("creating derived scene scene_rainbowwheel");
  }
  //DESTRUCTOR
  virtual ~scene_rainbowwheel(){
    delete _n;
    _n = NULL;
    Serial.println("deleting derived scene scene_rainbowwheel");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateRainbowJSON(sub_obj); 
  }
  //SHOW
  virtual void show_scene(){
    _k = map(_n->getcnt(),0,_n_max,0,(_num_leds-1)); // k <300 //k iterates the position of the first rainbowcolor periodically though the stripe 
    for(uint16_t i = 0; i < _num_leds; i++){
      _m            = (_k+i)%(_num_leds);
      _hue          = map(i, 0, (_num_leds-1), 0, 255);
      _hsv.h        = _hue;
      _hsv.v        = _bright;
      _leds_ptr[_m] = _hsv; 
    }
    FastLED.show();
  }  
};
