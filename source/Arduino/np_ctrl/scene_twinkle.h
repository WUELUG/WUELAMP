// ================================================================================
// ========== light scene TWINKLE =================================================

/*
##parameters:##
base brightness [%]
base color [R,G,B]
flash brightness [%]
flash color [R,G,B] (optional)

##description:##
All LEDs glow with a pretty low brightness1 (e.g. 10%) in a defined color1. 
Randomly chosen LEDs twinkle/flash with a higher brightness2. if a different color 
is provided as color2, flashing takes place randomly alternating in color1 and color2. 
period is not really a period in a classic understanding, it defines the time elapsing 
before a new flash follows the preceding one. As the length of the stipe has no 
impact on the period, the same value might seem a bit hectic for short stipes while 
creating a calm scene for long stripes as intended.

##application:##
this scene could e.g. be used as continuous state when test bench is turned off and room is dark*/
#define DELTA_FLASH_BRIGHT 30
#define FADE_UP_TICS 25
#define FADE_DOWN_TICS 35
#define MAX_ACTIVE_TICS 50
#define MIN_ACTIVE_TICS 10

class scene_twinkle : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period         = 200;
  uint8_t _base_bright  = 10;// internal brightness in 0..255, API in %!
  uint8_t _flash_bright = 255;// internal brightness in 0..255, API in %
  CRGB _rgb_standard    = 0xFF0000;
  CRGB _rgb_special     = 0x0000FF;
   
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max;
  uint8_t* _activeticks;   // array containing number of ticks, during which the indexed LED will remain active at high (alternating) brightness
  uint8_t* _fadeupticks;   // array containing number of ticks, during which the indexed  LED will remain active fading up brightness
  uint8_t* _fadedownticks; // array containing number of ticks, during which the indexed  LED will remain active fading down brightness
  uint8_t* _colorindex;    // array containing the index of predefined colors for the indexed LED 
  uint8_t _LowerFlashBright     = 0; 
  CRGB _colors[2];
  uint16_t _randindex           = 0;
  uint8_t _randactiveticks      = 0;
  uint8_t _randcolor            = 0;
  uint8_t _base_bright_percent  = 50; // internal brightness in 0..255, API in %!
  uint8_t _flash_bright_percent = 50; // internal brightness in 0..255, API in %! 
  
  //private JSON deserialization
  bool UpdateTwinkleJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period               = doc["period"] | _T_period;  // no period, force manual reset
    _base_bright_percent    = doc["brightness1"] | _base_bright_percent;
    _base_bright            = map(_base_bright_percent, 0,100,0,255);
    _flash_bright_percent   = doc["brightness2"] | _flash_bright_percent;
    _flash_bright           = map(_flash_bright_percent, 0,100,0,255);
    const char* _ColorType  = doc["color1"]["color_type"];
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb_standard = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb_standard = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb_standard = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb_standard[i]    = doc["color1"]["colorcode_RGB"][i];
      };
    }
    _ColorType              = doc["color2"]["color_type"];
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb_special = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb_special = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb_special = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb_special[i]     = doc["color2"]["colorcode_RGB"][i];
      };
    }
    _LowerFlashBright   = _flash_bright - DELTA_FLASH_BRIGHT;
    _colors[0]          = _rgb_standard;
    _colors[1]          = _rgb_special;    
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
  
  public:
  //CONSTRUCTOR
  scene_twinkle(uint16_t _num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{_num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "twinkle");
    _n = new discreteTimer(); // no period, force manual reset
    UpdateTwinkleJSON(sub_obj);
    _activeticks        = new uint8_t[_num_leds];
    _fadeupticks        = new uint8_t[_num_leds];
    _fadedownticks      = new uint8_t[_num_leds];
    _colorindex         = new uint8_t[_num_leds];
    for(uint16_t i; i<_num_leds; i++){
      _activeticks[i]   = 0;
      _fadeupticks[i]   = 0;
      _fadedownticks[i] = 0;
      _colorindex[i]    = 0;
      _leds_ptr[i] = _rgb_standard;
    }
    Serial.println("creating derived scene scene_twinkle");
  }
  //DESTRUCTOR
  virtual ~scene_twinkle(){
    delete _n;
    delete _activeticks;
    delete _fadeupticks;
    delete _fadedownticks;
    delete _colorindex;
    _n              = NULL;
    _activeticks    = NULL;
    _fadeupticks    = NULL;
    _fadedownticks  = NULL;
    _colorindex     = NULL;
    
    Serial.println("deleting derived scene scene_twinkle");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateTwinkleJSON(sub_obj);
  }
  //SHOW
  virtual void show_scene(){   
    if(_n->getelapsedtime()>= _T_period){// set random values for a randomly selected LED at the beginning of each period
      //Serial.println("random");
      _n->resetcnt();
      _randindex       = random16(1, (_num_leds-2));
      _randactiveticks = random8(MIN_ACTIVE_TICS, MAX_ACTIVE_TICS);
      _randcolor       = random8(0,2);
      for (uint16_t i = (_randindex-1); i<=(_randindex+1); i++){
        if (_fadedownticks[i] == 0){ //only write into vectors if corresponding LED is not already active
          _activeticks[i]   = _randactiveticks;
          _fadeupticks[i]   = FADE_UP_TICS; 
          _fadedownticks[i] = FADE_DOWN_TICS;
          _colorindex[i]    = _randcolor;
        }
      }
    }
    //Serial.println("call");
    if(_n->tick()){
      /* timing measurement for debugging only
      _t = millis();
      //Serial.println(_t-_t_last);
      meanperiod[pindex] = _t-_t_last;
      pindex++;
      pindex = pindex%19;
      uint16_t sum = 0;
      for(int i = 0; i<20; i++){
        sum += meanperiod[i];
      }
      //Serial.print("mean=");Serial.println(sum/20);
      _t_last = _t;*/
      for(int i = 0; i < _num_leds; i++) // iterate through all LEDs of the stripe and decide whether it's fading up, is at high brightness, is fading down or remains at standard brightness
      {
        if (_fadeupticks[i]>0){
          _leds_ptr[i] = _colors[_colorindex[i]]; 
          _leds_ptr[i].nscale8(map(FADE_UP_TICS - _fadeupticks[i],0, FADE_UP_TICS, _base_bright, _LowerFlashBright));
          _fadeupticks[i]--;
        }
        else if (_activeticks[i]>0){
          _leds_ptr[i] = _colors[_colorindex[i]]; 
          _leds_ptr[i].nscale8(_LowerFlashBright + random8(0,_flash_bright-_LowerFlashBright));
          _activeticks[i]--;          
        }
        else if (_fadedownticks[i]>0){
          _leds_ptr[i] = _colors[_colorindex[i]]; 
          _leds_ptr[i].nscale8(map(FADE_DOWN_TICS - _fadedownticks[i],0, FADE_DOWN_TICS, _LowerFlashBright, _base_bright));
          _fadedownticks[i]--;
        }
        else{
          _leds_ptr[i] = _colors[0]; 
          _leds_ptr[i].nscale8(_base_bright);
        }
      }
      FastLED.show();
    } 
  }  
};
