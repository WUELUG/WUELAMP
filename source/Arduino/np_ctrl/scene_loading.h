// ================================================================================
// ========== light scene LOADING =================================================
/*
## parameters:##
max brightness [%]
color [R,G,B] - ok
period [ms] - ok
length [%] - ok
posdirection [0,1] - ok

##description:##
Running light in a defined color, max brightness and frequency. The "head" LED of the running light shines 
in max brightness, the "tail" LED is dark. The number of LEDs between head and tail is defined by length 
(% of total # of LEDs in stripe), they fade from max brightness to dark. the LEDs after the tail remain dark.
if length (in p.u.) is smaller than an integer divider (e.g. 1/2, 1/3, 1/4) an integer number of 
subsequent running lights fill the stripe (e.g. 2, 3, 4 running lights following each other).
Period defines the time the first running light's head needs to move through the entire stripe
direction defines if the head LED is running from beginning to the end (positive direction) or 
vice versa (negative direction). the direction doesn't change within the same scene.


##application:##
looks kind of like the Windows 10 hourglas indication that some process is in progress. could be used for test bench startup, loading parameters etc.
 */
class scene_loading : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period       = 3000;
  CRGB _rgb1          = 0xFF0000;
  uint16_t _length    = 1;
  uint8_t _max_bright = 128;// internal brightness in 0..255, API in %!  map(MAX_BRIGHT, 0,100,0,255);
  bool _posdirection  = true;

  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max;
  uint16_t _leader        = 0;
  uint8_t _numberOfFades  = 0;
  uint8_t _length_percent = 0;
  uint8_t _bright_percent = 0;
  uint16_t _startpointer  = 0;
  int16_t index           = 0;
  CRGB* _transpose_helper; 

  //private JSON deserialization
  bool UpdateProgressJSON(char *sub_obj) {
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period       = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max          = _n->getcntmax();  
    const char* _ColorType = doc["color1"]["color_type"]; // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb1 = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb1 = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb1 = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb1[i] = doc["color1"]["colorcode_RGB"][i] | _rgb1[i];
      };
    }
    _length_percent = doc["length"] | _length_percent;
    _length         = map(_length_percent, 0, 100, 1, _num_leds); // initialize length with 51% of num_leds

    _bright_percent = doc["brightness1"] | _bright_percent;
    _max_bright     = map(_bright_percent, 0,100,0,255);

    _posdirection   = doc["direction"] | _posdirection;
    _numberOfFades  = floor(_num_leds/_length);
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
 
  public:
  //CONSTRUCTOR
  scene_loading(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "loading");
    _n = new discreteTimer;
    UpdateProgressJSON(sub_obj);
    _transpose_helper = new CRGB[_num_leds];
    for(uint16_t i; i<_num_leds; i++){
      _transpose_helper[i] = CRGB::Black;
    }
    
    Serial.println("creating derived scene scene_loading");
  }
  //DESTRUCTOR
  virtual ~scene_loading(){
    delete _n;
    _n = NULL;
    delete _transpose_helper;
    _transpose_helper = NULL;
    Serial.println("deleting derived scene scene_loading");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateProgressJSON(sub_obj);
  }
  //SHOW  
  virtual void show_scene(){
    int count = _n->getcnt();
    //Serial.print("count");Serial.println(count);
    _startpointer = map(count, 0,_n_max, 0, (_num_leds-1));
    for (uint8_t i = 0; i < _numberOfFades; i++){
      _leader = (_startpointer+i*_num_leds/_numberOfFades)%(_num_leds-1);
      for (uint16_t j = 0; j < _length; j++){
        int16_t index = _leader - j;
        if(index < 0){
          index = _num_leds + index;
        }
         _leds_ptr[index] = _rgb1; // _colors[i%3];
         _leds_ptr[index].nscale8(map(j,0,(_length-1),_max_bright,0)); 
         if(j==_length-1){
          index = _leader - (j+1);
          if(index < 0){
            index = _num_leds + index;
          }
          _leds_ptr[index] = CRGB::Black; // make sure the last one is really black
         }
         _transpose_helper[index] = _leds_ptr[index]; // in case the other direction is required, be prepared and store the values in a helper array
      }
    }
    if(!_posdirection){
      for(uint16_t i = 0; i < _num_leds; i++){
        _leds_ptr[i] = _transpose_helper[_num_leds-1-i];
      }
    }
    FastLED.show();
  }  
};


  
