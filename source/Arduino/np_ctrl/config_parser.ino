// ================================================================================
// ========== configuration and specific JSON pasring =============================

// This code manages reading and writing configurations from and to the SC card

#define CONF_BUFFER_SIZE 1024
#define DEFAULTS_BUFFER_SIZE 512
#define STRIP_BUFFER_SIZE 512
#define PROGRESS_BUFFER_SIZE 512

// ================================================================================
// ========== read all configs and load initial stuff =============================

void readConfigfromSD(t_network_config* _network){
  char      config_buffer[CONF_BUFFER_SIZE]       = "";
  char      defaults_buffer[DEFAULTS_BUFFER_SIZE] = "";
  char      strip_buffer[STRIP_BUFFER_SIZE]       = "";
  char      progress_buffer[STRIP_BUFFER_SIZE]    = "";
  char      Payload[1024]                         = "";
  if (!SD.begin(SDCARD_SS_PIN)) {
    Serial.println("initialization failed!");
    while (1); //makes no sense to go ahead
  }
  else{
    readConfFile(config_buffer, "config.txt");
    readConfFile(Payload, "iniscn.txt");
    readConfFile(defaults_buffer, "defaults.txt");
    readConfFile(strip_buffer, "strip.txt");
    readConfFile(progress_buffer, "progress.txt");
    //SD.end();//gibt es den befehl?
  }
  
  
  CheckJSONPayload(Payload, &Payload_Analysis, NewCommand, NewParams);
  // analysePayload is executed in calling function ( setup() ) to make sure all parameters are configured
  
  Serial.println("deserializing JSON");
  StaticJsonDocument<CONF_BUFFER_SIZE> config_json;
  DeserializationError error_conf = deserializeJson(config_json, config_buffer);
  if (error_conf) {
    Serial.print(F("config deserializeJson() failed with code "));
    Serial.println(error_conf.c_str());
  }
  StaticJsonDocument<DEFAULTS_BUFFER_SIZE> defaults_json;
  DeserializationError error_defaults = deserializeJson(defaults_json, defaults_buffer);
  if (error_defaults) {
    Serial.print(F("defaults deserializeJson() failed with code "));
    Serial.println(error_defaults.c_str());
  }
  StaticJsonDocument<STRIP_BUFFER_SIZE> strip_json;
  DeserializationError error_strip = deserializeJson(strip_json, strip_buffer);
  if (error_strip) {
    Serial.print(F("strip deserializeJson() failed with code "));
    Serial.println(error_strip.c_str());
  }
  
  StaticJsonDocument<PROGRESS_BUFFER_SIZE> progress_json;
  DeserializationError error_progress = deserializeJson(progress_json, progress_buffer);
  if (error_progress) {
    Serial.print(F("progress deserializeJson() failed with code "));
    Serial.println(error_progress.c_str());
  }

  //read from SD:
  strcpy(_network->mac,   config_json["network"]["mac_address"]);
  strcpy(_network->ip,    config_json["network"]["static_ipv4_settings"]["ip"]);
  strcpy(mqtt.broker_ip,  config_json["mqtt_control"]["broker_ip"]);
  strcpy(mqtt.client_id,  config_json["mqtt_control"]["client_id"]);
  strcpy(mqtt.user,       config_json["mqtt_control"]["user"]);
  strcpy(mqtt.password,   config_json["mqtt_control"]["password"]);
  //strcpy(strip.strip_type,"WS2812"); // template parameter, must be fix at compile time 
  //strip.data_pin = 0; // template parameter, must be fix at compile time 
  //strcpy(strip.color_order,"GRB"); // template parameter, must be fix at compile time 
  strip.num_leds          = strip_json["strip"][0]["num_leds"];
  strip.max_current_mA    = strip_json["strip"][0]["max_current_mA"];
  for (int i = 0; i <= 2; i++) {
    strip.correction[i] = strip_json["strip"][0]["correction"]["colorcode_RGB"][i];
  }
  //Serial.println(strip.num_leds );
  //Serial.println(strip.max_current_mA);

  for (int i = 0; i <= 2; i++) {
    defaultcolor.color1[i] = defaults_json["defaultcolors_123"]["defaultcolor_1"]["colorcode_RGB"][i];
    defaultcolor.color2[i] = defaults_json["defaultcolors_123"]["defaultcolor_2"]["colorcode_RGB"][i];
    defaultcolor.color3[i] = defaults_json["defaultcolors_123"]["defaultcolor_3"]["colorcode_RGB"][i];
    //Serial.println(defaultcolor.color1[i]);
  };

  progress_init.Begin1    = strip_json["strip"][0]["begin_1"];
  progress_init.End1      = strip_json["strip"][0]["end_1"];
  progress_init.End2      = strip_json["strip"][0]["end_2"];
  progress_init.Begin2    = strip_json["strip"][0]["begin_2"];
  progress_init.demo      = false;

  
  progress_init.lower_limit = progress_json["progress"]["lower_limit"];
  Serial.println(progress_init.lower_limit);
  progress_init.upper_limit = progress_json["progress"]["upper_limit"];
  Serial.println(progress_init.upper_limit);
  strcpy(progress_init.calc_mode, progress_json["progress"]["calc_mode"]);
  Serial.println(progress_init.calc_mode);
  progress_init.numberOfTopics = progress_json["progress"]["listen_on"].size();
  Serial.println(progress_init.numberOfTopics);
  progress_init.listen_on = new char*[progress_init.numberOfTopics];
  for(uint8_t i = 0; i < progress_init.numberOfTopics; i++){
    //Serial.println("i=");Serial.println(i);
    progress_init.listen_on[i] = new char[127];
    strcpy(progress_init.listen_on[i], progress_json["progress"]["listen_on"][i]);
    Serial.println(progress_init.listen_on[i]);
  }
  
  mqtt.numberOfTopics = config_json["mqtt_control"]["listen_on"].size();
  //Serial.print("topics: ");Serial.println(mqtt.numberOfTopics);  
  mqtt.listen_on = new char*[mqtt.numberOfTopics];
  for(uint8_t i = 0; i < mqtt.numberOfTopics; i++){
    mqtt.listen_on[i] = new char[127];
    strcpy(mqtt.listen_on[i], config_json["mqtt_control"]["listen_on"][i]);
    Serial.println(mqtt.listen_on[i]);
  }
}

// ================================================================================
// ========== read single config file =============================================

boolean readConfFile(char *_content, char *_filename){
  File file;
  file = SD.open(_filename);
  if (file) {
    Serial.print("reading ");
    Serial.println(_filename);
    // read from the file until there's nothing else in it:
    uint16_t i = 0;
    while (file.available()) {
      //Serial.write(configFile.read());
      _content[i] = file.read();
      i++;
    }
    //Serial.println(config_buffer);
    // close the file:
    file.close();
    return true;
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening");
    Serial.println(_filename);
    return false;
  }
}

// ================================================================================
// ========== write single config file ============================================

boolean rewriteConfOnSD(char *_content,char *_filename){
  //requries full filename incl. .txt
  //if existing, removes the named file and creates a new with the same name containing "_content"
  File file;
  if (!SD.begin(SDCARD_SS_PIN)) {
        Serial.println("initialization failed!");
        return false;
  }
  else{
    SD.remove(_filename);
    file = SD.open(_filename, FILE_WRITE);
    // if the file opened okay, write to it:
    if (file) {
      Serial.print("Writing to ");
      Serial.print(_filename);
      Serial.print("...");
      file.println(_content);
      // close the file:
      file.close();
      Serial.println("done.");
      return true;
    } 
    else {
      // if the file didn't open, print an error:
      Serial.print("error opening ");
      Serial.println(_filename);
      return false;
    }
  } 
}

// ================================================================================
// ========== translates ip from config to required format ========================

void parseIP(char *ip_str, byte* return_ip){ // copied from https://forum.arduino.cc/index.php?topic=497878.0, user: MarkT)
  int val = 0 ;  // temporary for building up value on the fly
  byte section = 0 ; // index into ip
  byte ip [4] ;
  
  int i = 0 ;
  while (true)
  {
    if (ip_str[i] == '.' || ip_str[i] == 0)  // section separator / end of string
    {
      ip [section++] = val ;  // store next byte
      if (ip_str[i] == 0 || section == 4)  // done if 4 sections parsed or end of string
        break ;
      val = 0 ;
    }
    else if ('0' <= ip_str[i] && ip_str[i] <= '9')
    {
      val = val * 10 + ip_str[i]-'0' ;  // build up value from decimal digits.
      if (val > 255)
        break ;
    }
    else
      break ;
    i++ ;  // step to next char
  }
  if (section == 4){ //valid IP
    //Serial.println("IP valid");
    for(int j = 0; j<4; j++){
      return_ip[j] = ip[j] ;     
    }
    //print IP
    Serial.print("parsed IP: ");
    for(int j = 0; j<4; j++){
      Serial.print(ip[j]);
      if(j<3){
        Serial.print(".");      
      }
      else{
        Serial.println();
      }
    }
  }
  else{// invalid IP
    Serial.println("IP invalid");
  }
}
// ================================================================================
// ========== translates mac from config to required format =======================

void parseMAC(char *mac_str, byte* return_mac){ // copied from https://forum.arduino.cc/index.php?topic=497878.0, user: MarkT)
  //accepts ":" or "-" as section separator
  //case in-sensitive for 0x letters
  int val = 0 ;  // temporary for building up value on the fly
  byte section = 0 ; // index into ip
  byte mac [4] ;
  
  int i = 0 ;
  while (true)
  {
    if (mac_str[i] == '-' ||mac_str[i] == ':' || mac_str[i] == 0)  // section separator / end of string
    {
      mac [section++] = val ;  // store next byte
      if (mac_str[i] == 0 || section == 6)  // done if 4 sections parsed or end of string
        break ;
      val = 0 ;
    }
    else if ('0' <= mac_str[i] && mac_str[i] <= '9')
    {
      //Serial.print("i= ");Serial.print(i);Serial.print(": ");Serial.println((byte)mac_str[i]);
      val = val * 16 + mac_str[i]-'0' ;  // build up value from decimal digits.
      //Serial.print("val= ");Serial.println(val);
      if (val > 255)
        break ;
    }
    else if ('A' <= mac_str[i] && mac_str[i] <= 'F')
    {
      //Serial.print("i= ");Serial.print(i);Serial.print(": ");Serial.println((byte)mac_str[i]);
      val = val * 16 + mac_str[i]-'A'+10 ;  // build up value from decimal digits.
      //Serial.print("val= ");Serial.println(val);
      if (val > 255)
        break ;
    }
    else if ('a' <= mac_str[i] && mac_str[i] <= 'f')
    {
      //Serial.print("i= ");Serial.print(i);Serial.print(": ");Serial.println((byte)mac_str[i]);
      val = val * 16 + mac_str[i]-'a'+10 ;  // build up value from decimal digits.
      //Serial.print("val= ");Serial.println(val);
      if (val > 255)
        break ;
    }
    else
      break ;
    i++ ;  // step to next char
  }
  if (section == 6){ //valid mac
    //Serial.println("mac valid");
    for(int j = 0; j<6; j++){
      return_mac[j] = mac[j] ;     
    }
    //print mac
    Serial.print("parsed mac: ");
    for(int j = 0; j<6; j++){
      Serial.print(mac[j]);
      if(j<5){
        Serial.print(":");      
      }
      else{
        Serial.println();
      }
    }
  }
  else{// invalid mac
    Serial.println("mac invalid");
  }
}
