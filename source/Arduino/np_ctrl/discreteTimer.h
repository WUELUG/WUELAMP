// ================================================================================
// ======= timer class offering different method for usage in the light scenes ====

class discreteTimer{
   private: 
   int delta_t = 20; // represents the (minimal) time increment of the discrete timin timing steps. make sure the complete code can be executed within delta_t
   unsigned long t_last = 0; //time stamp of last time increment
   int cnt = 0; //(resettable) counter representing the elapsed time cnt*delta_t since last reset
   int T_period = 0; // period in ms can be set to a value != 0 to reset cnt automatically. if T_period == 0, cnt must be reset externally
   bool cnt_inc = false;
   unsigned long error = 0;

   public:
   void setperiod(int T_ms){//T_period can be set optionally in order to reset cnt automatically. T_period = 0 --> reset cnt externally
    T_period = T_ms; 
   }
   //CONSTRUCTOR
   discreteTimer(){
    resetTimer();
    //Serial.println("initialize timer");
   }
   //DESTRUCTOR
   ~discreteTimer(){
    //Serial.println("delete timer");
   }
   
   int getcnt(){ //returns the current value of a counter cnt which represents a multiple of the time delta_t. if T_period != 0 cnt is reset automatically
    unsigned long dt = (millis()-t_last);
    //Serial.print("tlast=");Serial.println(t_last);
    //Serial.print("dt=");Serial.println(dt);
    if(dt+error >= delta_t){
      if(dt>delta_t){
        error = dt-delta_t;
      }
      else{
        error = 0;
      }
      //Serial.print("error=");;Serial.println(error);
      cnt_inc = true;
      cnt++;// = cnt + floor(dt/delta_t);
      t_last = millis();
      if (T_period >0){
        if (getelapsedtime()>=T_period){
          resetcnt();        
        }
      }
    }
    else{
      cnt_inc = false;
    }
    return cnt;
   }
   
   bool tick(){
    getcnt();
    delay(1);
    return cnt_inc;
   }

   int getcntmax(){ // returns the max value cnt achieves within a period specified by T_period
    return floor(T_period/delta_t);
   }
   
   void resetcnt(){// resets cnt via external call
    cnt = 0;
   }
   void resetTimer(){
    t_last = millis();
    //Serial.println("reset timer");
    //Serial.print("t_lastonreset=");Serial.println(t_last);
   }  
   
   int getelapsedtime(){
    return cnt*delta_t;
   }
   int getdelta_t(){
    return delta_t;
   }
};
