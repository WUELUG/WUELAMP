// ================================================================================
// ========== light scene TRICOLORWHEEL =================================================

/*
## parameters:##
period [ms]
brightness [%]
color1 [R,G,B]
color2 [R,G,B]
color3 [R,G,B]

##description:##
This scene divides the stripe into three sections iterating through the stripe at brightness1. 
Each section can have a different color (color1, color2, color3) the first LED of the first 
section (and all following ones) move through the entire stripe once during one period

##application:##
 */
class scene_tricolorwheel : public scene{
  private:
  //specific parameters, remote controllable
  int _T_period   = 5000;
  uint8_t _bright = 255; // internal brightness in 0..255, API in %! 
  CRGB _rgb1      = 0xFF0000; //(0, 153, 153)   Siemens AG
  CRGB _rgb2      = 0x00FF00;  //(138, 0, 229)   Siemens Energy Bright Purple
  CRGB _rgb3      = 0x0000FF; //(242, 112, 33)  Siemens healthineers
   
  //helper variables
  discreteTimer* _n; // create timer object
  int _n_max;
  uint8_t _bright_percent = 100; 
  uint16_t _startpointer  = 0; // _startpointer iterates the position of the first rainbowcolor periodically though the stripe
  uint16_t _index         = 0; 
  
  //private JSON deserialization
  bool UpdateTriColJSON(char *sub_obj){
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, sub_obj);
    if (error) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(error.c_str());
      // Abbruch 
      return false;
    }
    _T_period               = doc["period"] | _T_period;
    _n->setperiod(_T_period);
    _n_max                  = _n->getcntmax();
    _bright_percent         = doc["brightness1"] | _bright_percent;
    _bright                 = map(_bright_percent, 0,100,0,255);
    const char* _ColorType  = doc["color1"]["color_type"]; // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb1 = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb1 = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb1 = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb1[i]            = doc["color1"]["colorcode_RGB"][i];
      };
    }
    _ColorType              = doc["color2"]["color_type"]; // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb2 = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb2 = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb2 = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb2[i]            = doc["color2"]["colorcode_RGB"][i];
      };
    }
    _ColorType              = doc["color3"]["color_type"]; // schöner wäre es, daraus auch noch mal ne Funktion zu bauen...
    if (strcmp(_ColorType, "defaultcolor_1") == 0) _rgb3 = _defaultcolor_ptr->color1;
    if (strcmp(_ColorType, "defaultcolor_2") == 0) _rgb3 = _defaultcolor_ptr->color2;
    if (strcmp(_ColorType, "defaultcolor_3") == 0) _rgb3 = _defaultcolor_ptr->color3;
    if (strcmp(_ColorType, "custom") == 0){
      for (int i = 0; i <= 2; i++) {
        _rgb3[i]            = doc["color3"]["colorcode_RGB"][i];
      };
    }
    return true;    // evtl. nur true, wenn auch min. ein Wert geändert wurde?
  }
  
  public:
  //CONSTRUCTOR
  scene_tricolorwheel(uint16_t num_leds_in, CRGB *leds_ptr, t_defaultcolor *defaultcolor_ptr, char *sub_obj)
    : scene{num_leds_in, leds_ptr, defaultcolor_ptr} // set base class members
  {
    strcpy(_active_scene, "tricolor");
    _n = new discreteTimer;
    UpdateTriColJSON(sub_obj);
    _n->setperiod(_T_period); //möglicherweise einen globalen zähler instanziieren und prt darauf an die aktive szene übergeben
    _n_max    = _n->getcntmax();
    Serial.println("creating derived scene scene_tricolorwheel");
  }
  //DESTRUCTOR
  virtual ~scene_tricolorwheel(){
    delete _n;
    _n = NULL;
    Serial.println("deleting derived scene scene_tricolorwheel");
  }
  //SETTER
  virtual void update_scene(char *sub_obj){
    UpdateTriColJSON(sub_obj); 
  }
  //SHOW
  virtual void show_scene(){
    _startpointer = map(_n->getcnt(),0,_n_max,0,(_num_leds-1));
    for(uint16_t i = 0; i < _num_leds/3; i++){
      _index = (_startpointer+i)%(_num_leds);
      _leds_ptr[_index] = _rgb1;
      _leds_ptr[_index].nscale8(_bright); 
    }
    for(uint16_t i = _num_leds/3; i < 2*_num_leds/3; i++){
      _index = (_startpointer+i)%(_num_leds);
      _leds_ptr[_index] = _rgb2;
      _leds_ptr[_index].nscale8(_bright); 
    }
      for(uint16_t i = 2*_num_leds/3; i < _num_leds; i++){
      _index = (_startpointer+i)%(_num_leds);
      _leds_ptr[_index] = _rgb3;
      _leds_ptr[_index].nscale8(_bright); 
    }
    FastLED.show();
  }  
};
