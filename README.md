# WUELAMP: WUELUG LabVIEW Arduino Multi-LED Pixels

API and demo for accessing [Adafruit's NeoPixel LEDs](https://www.adafruit.com/category/168) via [Arduino controllers](https://www.arduino.cc) from [NI LabVIEW(tm)](https://www.ni.com/en-us/shop/labview.html).

## :pencil: Documentation

All available documentation can be found in the project wiki at https://gitlab.com/WUELUG/WUELAMP/-/wikis/

## :rocket: Installation

### :building_construction: Hardware

Even though the code should be executable on various Arduino controllers (and has been tested on some!), our harware of choice is as follows:

-  Arduino MKR Zero + Arduino MKR ETH Shield

### :wrench: LabVIEW 2018

The LabVIEW VIs are maintained in LabVIEW 2018.

### :link: Dependencies

For LabVIEW: Install the VI Package configuration file found at `/Source/LabVIEW/WUELAMP.vipc`. 

For Arduino: See the #include defitions in the .ino files:

- `SPI.h`
- `Ethernet.h`
- `EthernetUdp.h`
- `FastLED.h`



## :bulb: Usage

Run the UDP Test sketch on your Arduino and the UDP Test VI on your PC. Make sure to update IP addresses according to your local setup.

## :construction: Maintenance 

This repository is maintained by [HAMPEL SOFTWARE ENGINEERING](https://www.hampel-soft.com) on behalf of the [Wuerzburg LabVIEW User Group](http://bit.ly/WUELUG). 


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Sebastian F. Kleinau
* Thorsten Halsch
* Julian Lange (Siemens Energy)
* Sebastian F. Kleinau
* Thorsten Halsch (Siemens Healthcare)
* Bence Bartho (Hampel Software Engineering)
* Joerg Hampel (Hampel Software Engineering)


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
